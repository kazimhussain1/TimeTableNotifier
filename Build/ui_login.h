/********************************************************************************
** Form generated from reading UI file 'login.ui'
**
** Created by: Qt User Interface Compiler version 5.11.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGIN_H
#define UI_LOGIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>

QT_BEGIN_NAMESPACE

class Ui_Login
{
public:
    QPushButton *Signup;
    QTextEdit *textEdit;
    QLabel *label_3;
    QGroupBox *gridGroupBox;
    QGridLayout *gridLayout;
    QGroupBox *groupBox;
    QLineEdit *Pass;
    QLabel *label_2;
    QPushButton *F_pass;
    QLineEdit *Email;
    QLabel *label;
    QPlainTextEdit *Info;
    QPushButton *Log_in;

    void setupUi(QDialog *Login)
    {
        if (Login->objectName().isEmpty())
            Login->setObjectName(QStringLiteral("Login"));
        Login->setEnabled(true);
        Login->resize(1000, 650);
        QFont font;
        font.setBold(false);
        font.setWeight(50);
        Login->setFont(font);
        Login->setCursor(QCursor(Qt::ArrowCursor));
        Signup = new QPushButton(Login);
        Signup->setObjectName(QStringLiteral("Signup"));
        Signup->setGeometry(QRect(850, 540, 75, 23));
        QPalette palette;
        QBrush brush(QColor(3, 39, 157, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(0, 4, 240, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        QBrush brush2(QColor(105, 107, 255, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Light, brush2);
        QBrush brush3(QColor(52, 55, 247, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush3);
        QBrush brush4(QColor(0, 2, 120, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Dark, brush4);
        QBrush brush5(QColor(1, 14, 200, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Mid, brush5);
        QBrush brush6(QColor(0, 3, 186, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Text, brush6);
        QBrush brush7(QColor(255, 255, 255, 255));
        brush7.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::BrightText, brush7);
        QBrush brush8(QColor(1, 4, 170, 255));
        brush8.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush8);
        palette.setBrush(QPalette::Active, QPalette::Base, brush7);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        QBrush brush9(QColor(0, 2, 154, 255));
        brush9.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Shadow, brush9);
        QBrush brush10(QColor(127, 129, 247, 255));
        brush10.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::AlternateBase, brush10);
        QBrush brush11(QColor(255, 255, 220, 255));
        brush11.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ToolTipBase, brush11);
        QBrush brush12(QColor(3, 13, 153, 255));
        brush12.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush12);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Inactive, QPalette::Mid, brush5);
        QBrush brush13(QColor(0, 0, 0, 255));
        brush13.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush13);
        palette.setBrush(QPalette::Inactive, QPalette::BrightText, brush7);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush8);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush7);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Shadow, brush9);
        palette.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush10);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush11);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush12);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::BrightText, brush7);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Shadow, brush9);
        palette.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush11);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush12);
        Signup->setPalette(palette);
        QFont font1;
        font1.setPointSize(9);
        font1.setBold(true);
        font1.setUnderline(true);
        font1.setWeight(75);
        font1.setStrikeOut(false);
        Signup->setFont(font1);
        Signup->setCursor(QCursor(Qt::PointingHandCursor));
        Signup->setMouseTracking(true);
        Signup->setTabletTracking(true);
        Signup->setFocusPolicy(Qt::StrongFocus);
        Signup->setAutoDefault(false);
        Signup->setFlat(true);
        textEdit = new QTextEdit(Login);
        textEdit->setObjectName(QStringLiteral("textEdit"));
        textEdit->setEnabled(true);
        textEdit->setGeometry(QRect(780, 520, 261, 31));
        QPalette palette1;
        QBrush brush14(QColor(240, 240, 240, 255));
        brush14.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Base, brush14);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush14);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush14);
        textEdit->setPalette(palette1);
        textEdit->setFocusPolicy(Qt::NoFocus);
        textEdit->setFrameShape(QFrame::NoFrame);
        textEdit->setFrameShadow(QFrame::Plain);
        label_3 = new QLabel(Login);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(310, 120, 141, 71));
        QPalette palette2;
        QBrush brush15(QColor(0, 0, 127, 255));
        brush15.setStyle(Qt::SolidPattern);
        palette2.setBrush(QPalette::Active, QPalette::WindowText, brush15);
        palette2.setBrush(QPalette::Active, QPalette::Text, brush13);
        palette2.setBrush(QPalette::Active, QPalette::ButtonText, brush13);
        palette2.setBrush(QPalette::Inactive, QPalette::WindowText, brush15);
        palette2.setBrush(QPalette::Inactive, QPalette::Text, brush13);
        palette2.setBrush(QPalette::Inactive, QPalette::ButtonText, brush13);
        QBrush brush16(QColor(120, 120, 120, 255));
        brush16.setStyle(Qt::SolidPattern);
        palette2.setBrush(QPalette::Disabled, QPalette::WindowText, brush16);
        palette2.setBrush(QPalette::Disabled, QPalette::Text, brush16);
        palette2.setBrush(QPalette::Disabled, QPalette::ButtonText, brush16);
        label_3->setPalette(palette2);
        QFont font2;
        font2.setFamily(QStringLiteral("Palatino Linotype"));
        font2.setPointSize(33);
        font2.setBold(true);
        font2.setItalic(false);
        font2.setUnderline(true);
        font2.setWeight(75);
        label_3->setFont(font2);
        label_3->setWordWrap(false);
        gridGroupBox = new QGroupBox(Login);
        gridGroupBox->setObjectName(QStringLiteral("gridGroupBox"));
        gridGroupBox->setGeometry(QRect(310, 200, 421, 321));
        gridLayout = new QGridLayout(gridGroupBox);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        groupBox = new QGroupBox(gridGroupBox);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        Pass = new QLineEdit(groupBox);
        Pass->setObjectName(QStringLiteral("Pass"));
        Pass->setGeometry(QRect(100, 150, 211, 31));
        Pass->setTabletTracking(true);
        Pass->setEchoMode(QLineEdit::Password);
        Pass->setClearButtonEnabled(false);
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(100, 130, 71, 16));
        QFont font3;
        font3.setPointSize(9);
        font3.setBold(true);
        font3.setWeight(75);
        label_2->setFont(font3);
        F_pass = new QPushButton(groupBox);
        F_pass->setObjectName(QStringLiteral("F_pass"));
        F_pass->setGeometry(QRect(100, 180, 111, 23));
        QPalette palette3;
        QBrush brush17(QColor(0, 12, 124, 255));
        brush17.setStyle(Qt::SolidPattern);
        palette3.setBrush(QPalette::Active, QPalette::ButtonText, brush17);
        palette3.setBrush(QPalette::Inactive, QPalette::ButtonText, brush17);
        palette3.setBrush(QPalette::Disabled, QPalette::ButtonText, brush16);
        F_pass->setPalette(palette3);
        QFont font4;
        font4.setUnderline(true);
        F_pass->setFont(font4);
        F_pass->setCursor(QCursor(Qt::PointingHandCursor));
        F_pass->setAutoDefault(false);
        F_pass->setFlat(true);
        Email = new QLineEdit(groupBox);
        Email->setObjectName(QStringLiteral("Email"));
        Email->setGeometry(QRect(100, 70, 211, 31));
        Email->setEchoMode(QLineEdit::Normal);
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(100, 50, 71, 16));
        label->setFont(font3);
        Info = new QPlainTextEdit(groupBox);
        Info->setObjectName(QStringLiteral("Info"));
        Info->setEnabled(true);
        Info->setGeometry(QRect(100, 180, 211, 31));
        QPalette palette4;
        QBrush brush18(QColor(255, 0, 0, 255));
        brush18.setStyle(Qt::SolidPattern);
        palette4.setBrush(QPalette::Active, QPalette::WindowText, brush18);
        palette4.setBrush(QPalette::Active, QPalette::Light, brush7);
        palette4.setBrush(QPalette::Active, QPalette::Text, brush18);
        palette4.setBrush(QPalette::Active, QPalette::BrightText, brush7);
        palette4.setBrush(QPalette::Active, QPalette::ButtonText, brush18);
        palette4.setBrush(QPalette::Active, QPalette::Base, brush14);
        palette4.setBrush(QPalette::Active, QPalette::HighlightedText, brush7);
        palette4.setBrush(QPalette::Inactive, QPalette::WindowText, brush18);
        palette4.setBrush(QPalette::Inactive, QPalette::Light, brush7);
        palette4.setBrush(QPalette::Inactive, QPalette::Text, brush18);
        palette4.setBrush(QPalette::Inactive, QPalette::BrightText, brush7);
        palette4.setBrush(QPalette::Inactive, QPalette::ButtonText, brush18);
        palette4.setBrush(QPalette::Inactive, QPalette::Base, brush14);
        palette4.setBrush(QPalette::Inactive, QPalette::HighlightedText, brush7);
        palette4.setBrush(QPalette::Disabled, QPalette::WindowText, brush16);
        palette4.setBrush(QPalette::Disabled, QPalette::Light, brush7);
        palette4.setBrush(QPalette::Disabled, QPalette::Text, brush16);
        palette4.setBrush(QPalette::Disabled, QPalette::BrightText, brush7);
        palette4.setBrush(QPalette::Disabled, QPalette::ButtonText, brush16);
        palette4.setBrush(QPalette::Disabled, QPalette::Base, brush14);
        palette4.setBrush(QPalette::Disabled, QPalette::HighlightedText, brush7);
        Info->setPalette(palette4);
        QFont font5;
        font5.setBold(true);
        font5.setUnderline(false);
        font5.setWeight(75);
        Info->setFont(font5);
        Info->setFocusPolicy(Qt::NoFocus);
        Info->setFrameShape(QFrame::NoFrame);
        Info->setFrameShadow(QFrame::Plain);
        Info->setReadOnly(true);
        Info->setBackgroundVisible(true);
        Log_in = new QPushButton(groupBox);
        Log_in->setObjectName(QStringLiteral("Log_in"));
        Log_in->setGeometry(QRect(170, 250, 81, 31));
        QFont font6;
        font6.setBold(true);
        font6.setWeight(75);
        Log_in->setFont(font6);
        Log_in->setCursor(QCursor(Qt::PointingHandCursor));
        Log_in->setMouseTracking(true);
        Log_in->setTabletTracking(true);
        Info->raise();
        Pass->raise();
        label_2->raise();
        F_pass->raise();
        Email->raise();
        label->raise();
        Log_in->raise();

        gridLayout->addWidget(groupBox, 0, 0, 1, 1);

        textEdit->raise();
        Signup->raise();
        label_3->raise();
        gridGroupBox->raise();
        QWidget::setTabOrder(Email, Pass);
        QWidget::setTabOrder(Pass, F_pass);
        QWidget::setTabOrder(F_pass, Log_in);
        QWidget::setTabOrder(Log_in, Signup);

        retranslateUi(Login);

        F_pass->setDefault(false);


        QMetaObject::connectSlotsByName(Login);
    } // setupUi

    void retranslateUi(QDialog *Login)
    {
        Login->setWindowTitle(QApplication::translate("Login", "Login", nullptr));
        Signup->setText(QApplication::translate("Login", "SIGNUP", nullptr));
        textEdit->setHtml(QApplication::translate("Login", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600; text-decoration: underline; color:#767676;\">Would you want to create a profile?</span></p></body></html>", nullptr));
        label_3->setText(QApplication::translate("Login", "Login:", nullptr));
        groupBox->setTitle(QString());
        label_2->setText(QApplication::translate("Login", "Password:", nullptr));
        F_pass->setText(QApplication::translate("Login", "Forgotten Password?", nullptr));
        label->setText(QApplication::translate("Login", "Email:", nullptr));
        Info->setPlainText(QApplication::translate("Login", "Invalid username or Password...", nullptr));
        Log_in->setText(QApplication::translate("Login", "LOGIN", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Login: public Ui_Login {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGIN_H
