/********************************************************************************
** Form generated from reading UI file 'forget_pass.ui'
**
** Created by: Qt User Interface Compiler version 5.11.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORGET_PASS_H
#define UI_FORGET_PASS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_forget_pass
{
public:
    QWidget *centralwidget;
    QGroupBox *gridGroupBox;
    QGridLayout *gridLayout;
    QGroupBox *groupBox;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *Email;
    QLineEdit *C_no;
    QPlainTextEdit *plainTextEdit;
    QLabel *label_3;
    QLabel *label_4;
    QLineEdit *A_info;
    QPushButton *F_acc;
    QLabel *label_Email;
    QLabel *label_C_no;
    QLabel *label_A_Info;
    QGroupBox *gridGroupBox1;
    QGridLayout *gridLayout_2;
    QGroupBox *groupBox_2;
    QPlainTextEdit *plainTextEdit_2;
    QLabel *label_5;
    QPushButton *S_code;
    QLineEdit *Code;
    QPlainTextEdit *plainTextEdit_3;
    QPlainTextEdit *C_num;
    QPushButton *A_C_send;
    QGroupBox *gridGroupBox2;
    QGridLayout *gridLayout_3;
    QGroupBox *groupBox_3;
    QLabel *label_6;
    QPlainTextEdit *plainTextEdit_5;
    QLabel *label_7;
    QLabel *label_8;
    QPushButton *S_question;
    QLineEdit *A_que;
    QPlainTextEdit *S_que;
    QGroupBox *gridGroupBox3;
    QGridLayout *gridLayout_4;
    QGroupBox *groupBox_4;
    QPlainTextEdit *plainTextEdit_7;
    QLabel *label_9;
    QPushButton *N_password;
    QLineEdit *N_pass;
    QPushButton *P_hide;
    QLabel *label_10;
    QPushButton *P_show;

    void setupUi(QMainWindow *forget_pass)
    {
        if (forget_pass->objectName().isEmpty())
            forget_pass->setObjectName(QStringLiteral("forget_pass"));
        forget_pass->resize(1000, 700);
        centralwidget = new QWidget(forget_pass);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        gridGroupBox = new QGroupBox(centralwidget);
        gridGroupBox->setObjectName(QStringLiteral("gridGroupBox"));
        gridGroupBox->setGeometry(QRect(160, 10, 671, 201));
        gridLayout = new QGridLayout(gridGroupBox);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        groupBox = new QGroupBox(gridGroupBox);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        QFont font;
        font.setPointSize(8);
        groupBox->setFont(font);
        groupBox->setFlat(false);
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(60, 60, 41, 21));
        QFont font1;
        font1.setPointSize(9);
        font1.setBold(true);
        font1.setUnderline(true);
        font1.setWeight(75);
        label->setFont(font1);
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(60, 100, 81, 21));
        label_2->setFont(font1);
        Email = new QLineEdit(groupBox);
        Email->setObjectName(QStringLiteral("Email"));
        Email->setGeometry(QRect(160, 60, 211, 26));
        C_no = new QLineEdit(groupBox);
        C_no->setObjectName(QStringLiteral("C_no"));
        C_no->setGeometry(QRect(160, 100, 211, 26));
        plainTextEdit = new QPlainTextEdit(groupBox);
        plainTextEdit->setObjectName(QStringLiteral("plainTextEdit"));
        plainTextEdit->setGeometry(QRect(50, 34, 561, 31));
        QPalette palette;
        QBrush brush(QColor(240, 240, 240, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Base, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush);
        plainTextEdit->setPalette(palette);
        QFont font2;
        font2.setPointSize(9);
        plainTextEdit->setFont(font2);
        plainTextEdit->setFocusPolicy(Qt::NoFocus);
        plainTextEdit->setFrameShape(QFrame::NoFrame);
        plainTextEdit->setFrameShadow(QFrame::Plain);
        plainTextEdit->setReadOnly(true);
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(50, 4, 231, 31));
        QFont font3;
        font3.setPointSize(18);
        font3.setBold(true);
        font3.setUnderline(true);
        font3.setWeight(75);
        label_3->setFont(font3);
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(60, 140, 91, 21));
        label_4->setFont(font1);
        A_info = new QLineEdit(groupBox);
        A_info->setObjectName(QStringLiteral("A_info"));
        A_info->setGeometry(QRect(160, 140, 211, 26));
        F_acc = new QPushButton(groupBox);
        F_acc->setObjectName(QStringLiteral("F_acc"));
        F_acc->setGeometry(QRect(560, 150, 75, 31));
        QPalette palette1;
        QBrush brush1(QColor(0, 0, 127, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        QBrush brush2(QColor(120, 120, 120, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        F_acc->setPalette(palette1);
        QFont font4;
        font4.setPointSize(10);
        font4.setBold(true);
        font4.setUnderline(false);
        font4.setWeight(75);
        F_acc->setFont(font4);
        F_acc->setCursor(QCursor(Qt::PointingHandCursor));
        F_acc->setFlat(true);
        label_Email = new QLabel(groupBox);
        label_Email->setObjectName(QStringLiteral("label_Email"));
        label_Email->setGeometry(QRect(380, 65, 47, 13));
        QPalette palette2;
        QBrush brush3(QColor(255, 0, 0, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette2.setBrush(QPalette::Active, QPalette::WindowText, brush3);
        palette2.setBrush(QPalette::Inactive, QPalette::WindowText, brush3);
        palette2.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        label_Email->setPalette(palette2);
        label_Email->setFont(font2);
        label_C_no = new QLabel(groupBox);
        label_C_no->setObjectName(QStringLiteral("label_C_no"));
        label_C_no->setGeometry(QRect(380, 105, 47, 13));
        QPalette palette3;
        palette3.setBrush(QPalette::Active, QPalette::WindowText, brush3);
        palette3.setBrush(QPalette::Inactive, QPalette::WindowText, brush3);
        palette3.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        label_C_no->setPalette(palette3);
        label_C_no->setFont(font2);
        label_A_Info = new QLabel(groupBox);
        label_A_Info->setObjectName(QStringLiteral("label_A_Info"));
        label_A_Info->setGeometry(QRect(380, 146, 47, 13));
        QPalette palette4;
        palette4.setBrush(QPalette::Active, QPalette::WindowText, brush3);
        palette4.setBrush(QPalette::Inactive, QPalette::WindowText, brush3);
        palette4.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        label_A_Info->setPalette(palette4);
        label_A_Info->setFont(font2);
        plainTextEdit->raise();
        label->raise();
        label_2->raise();
        Email->raise();
        C_no->raise();
        label_3->raise();
        label_4->raise();
        A_info->raise();
        F_acc->raise();
        label_Email->raise();
        label_C_no->raise();
        label_A_Info->raise();

        gridLayout->addWidget(groupBox, 0, 0, 1, 1);

        gridGroupBox1 = new QGroupBox(centralwidget);
        gridGroupBox1->setObjectName(QStringLiteral("gridGroupBox1"));
        gridGroupBox1->setGeometry(QRect(160, 210, 671, 151));
        gridLayout_2 = new QGridLayout(gridGroupBox1);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        groupBox_2 = new QGroupBox(gridGroupBox1);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        plainTextEdit_2 = new QPlainTextEdit(groupBox_2);
        plainTextEdit_2->setObjectName(QStringLiteral("plainTextEdit_2"));
        plainTextEdit_2->setGeometry(QRect(40, 36, 491, 31));
        QPalette palette5;
        palette5.setBrush(QPalette::Active, QPalette::Base, brush);
        palette5.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette5.setBrush(QPalette::Disabled, QPalette::Base, brush);
        plainTextEdit_2->setPalette(palette5);
        plainTextEdit_2->setFont(font2);
        plainTextEdit_2->setFocusPolicy(Qt::NoFocus);
        plainTextEdit_2->setFrameShape(QFrame::NoFrame);
        plainTextEdit_2->setFrameShadow(QFrame::Plain);
        plainTextEdit_2->setReadOnly(true);
        label_5 = new QLabel(groupBox_2);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(40, 4, 251, 31));
        label_5->setFont(font3);
        S_code = new QPushButton(groupBox_2);
        S_code->setObjectName(QStringLiteral("S_code"));
        S_code->setGeometry(QRect(550, 100, 91, 31));
        QPalette palette6;
        palette6.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette6.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        palette6.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        S_code->setPalette(palette6);
        S_code->setFont(font4);
        S_code->setCursor(QCursor(Qt::PointingHandCursor));
        S_code->setFlat(true);
        Code = new QLineEdit(groupBox_2);
        Code->setObjectName(QStringLiteral("Code"));
        Code->setGeometry(QRect(100, 70, 211, 26));
        plainTextEdit_3 = new QPlainTextEdit(groupBox_2);
        plainTextEdit_3->setObjectName(QStringLiteral("plainTextEdit_3"));
        plainTextEdit_3->setGeometry(QRect(490, 60, 171, 31));
        QPalette palette7;
        palette7.setBrush(QPalette::Active, QPalette::Base, brush);
        palette7.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette7.setBrush(QPalette::Disabled, QPalette::Base, brush);
        plainTextEdit_3->setPalette(palette7);
        QFont font5;
        font5.setPointSize(9);
        font5.setBold(true);
        font5.setWeight(75);
        plainTextEdit_3->setFont(font5);
        plainTextEdit_3->setFocusPolicy(Qt::NoFocus);
        plainTextEdit_3->setFrameShape(QFrame::NoFrame);
        plainTextEdit_3->setFrameShadow(QFrame::Plain);
        plainTextEdit_3->setReadOnly(true);
        C_num = new QPlainTextEdit(groupBox_2);
        C_num->setObjectName(QStringLiteral("C_num"));
        C_num->setGeometry(QRect(520, 80, 91, 31));
        QPalette palette8;
        palette8.setBrush(QPalette::Active, QPalette::Base, brush);
        palette8.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette8.setBrush(QPalette::Disabled, QPalette::Base, brush);
        C_num->setPalette(palette8);
        C_num->setFont(font2);
        C_num->setFocusPolicy(Qt::NoFocus);
        C_num->setFrameShape(QFrame::NoFrame);
        C_num->setFrameShadow(QFrame::Plain);
        C_num->setReadOnly(true);
        A_C_send = new QPushButton(groupBox_2);
        A_C_send->setObjectName(QStringLiteral("A_C_send"));
        A_C_send->setGeometry(QRect(0, 100, 111, 31));
        QPalette palette9;
        palette9.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette9.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        palette9.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        A_C_send->setPalette(palette9);
        QFont font6;
        font6.setPointSize(10);
        font6.setBold(false);
        font6.setUnderline(true);
        font6.setWeight(50);
        A_C_send->setFont(font6);
        A_C_send->setCursor(QCursor(Qt::PointingHandCursor));
        A_C_send->setFlat(true);
        plainTextEdit_2->raise();
        label_5->raise();
        Code->raise();
        plainTextEdit_3->raise();
        C_num->raise();
        A_C_send->raise();
        S_code->raise();

        gridLayout_2->addWidget(groupBox_2, 0, 0, 1, 1);

        gridGroupBox2 = new QGroupBox(centralwidget);
        gridGroupBox2->setObjectName(QStringLiteral("gridGroupBox2"));
        gridGroupBox2->setGeometry(QRect(159, 360, 671, 161));
        gridLayout_3 = new QGridLayout(gridGroupBox2);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        groupBox_3 = new QGroupBox(gridGroupBox2);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        label_6 = new QLabel(groupBox_3);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(60, 110, 111, 21));
        label_6->setFont(font1);
        plainTextEdit_5 = new QPlainTextEdit(groupBox_3);
        plainTextEdit_5->setObjectName(QStringLiteral("plainTextEdit_5"));
        plainTextEdit_5->setGeometry(QRect(40, 35, 581, 31));
        QPalette palette10;
        palette10.setBrush(QPalette::Active, QPalette::Base, brush);
        palette10.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette10.setBrush(QPalette::Disabled, QPalette::Base, brush);
        plainTextEdit_5->setPalette(palette10);
        plainTextEdit_5->setFont(font2);
        plainTextEdit_5->setFocusPolicy(Qt::NoFocus);
        plainTextEdit_5->setFrameShape(QFrame::NoFrame);
        plainTextEdit_5->setFrameShadow(QFrame::Plain);
        plainTextEdit_5->setReadOnly(true);
        label_7 = new QLabel(groupBox_3);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(60, 70, 121, 21));
        label_7->setFont(font1);
        label_8 = new QLabel(groupBox_3);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(40, 3, 301, 31));
        label_8->setFont(font3);
        S_question = new QPushButton(groupBox_3);
        S_question->setObjectName(QStringLiteral("S_question"));
        S_question->setGeometry(QRect(550, 110, 91, 31));
        QPalette palette11;
        palette11.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette11.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        palette11.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        S_question->setPalette(palette11);
        S_question->setFont(font4);
        S_question->setCursor(QCursor(Qt::PointingHandCursor));
        S_question->setFlat(true);
        A_que = new QLineEdit(groupBox_3);
        A_que->setObjectName(QStringLiteral("A_que"));
        A_que->setGeometry(QRect(190, 110, 211, 26));
        S_que = new QPlainTextEdit(groupBox_3);
        S_que->setObjectName(QStringLiteral("S_que"));
        S_que->setGeometry(QRect(190, 70, 211, 31));
        QPalette palette12;
        palette12.setBrush(QPalette::Active, QPalette::Base, brush);
        palette12.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette12.setBrush(QPalette::Disabled, QPalette::Base, brush);
        S_que->setPalette(palette12);
        S_que->setFont(font2);
        S_que->setFocusPolicy(Qt::NoFocus);
        S_que->setFrameShape(QFrame::StyledPanel);
        S_que->setFrameShadow(QFrame::Plain);
        S_que->setReadOnly(true);

        gridLayout_3->addWidget(groupBox_3, 0, 0, 1, 1);

        gridGroupBox3 = new QGroupBox(centralwidget);
        gridGroupBox3->setObjectName(QStringLiteral("gridGroupBox3"));
        gridGroupBox3->setGeometry(QRect(160, 520, 671, 171));
        gridLayout_4 = new QGridLayout(gridGroupBox3);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        groupBox_4 = new QGroupBox(gridGroupBox3);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        plainTextEdit_7 = new QPlainTextEdit(groupBox_4);
        plainTextEdit_7->setObjectName(QStringLiteral("plainTextEdit_7"));
        plainTextEdit_7->setGeometry(QRect(40, 50, 591, 41));
        QPalette palette13;
        palette13.setBrush(QPalette::Active, QPalette::Base, brush);
        palette13.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette13.setBrush(QPalette::Disabled, QPalette::Base, brush);
        plainTextEdit_7->setPalette(palette13);
        plainTextEdit_7->setFont(font2);
        plainTextEdit_7->setFocusPolicy(Qt::NoFocus);
        plainTextEdit_7->setFrameShape(QFrame::NoFrame);
        plainTextEdit_7->setFrameShadow(QFrame::Plain);
        plainTextEdit_7->setReadOnly(true);
        label_9 = new QLabel(groupBox_4);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(40, 10, 271, 31));
        label_9->setFont(font3);
        N_password = new QPushButton(groupBox_4);
        N_password->setObjectName(QStringLiteral("N_password"));
        N_password->setGeometry(QRect(550, 120, 91, 31));
        QPalette palette14;
        palette14.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette14.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        palette14.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        N_password->setPalette(palette14);
        N_password->setFont(font4);
        N_password->setCursor(QCursor(Qt::PointingHandCursor));
        N_password->setFlat(true);
        N_pass = new QLineEdit(groupBox_4);
        N_pass->setObjectName(QStringLiteral("N_pass"));
        N_pass->setGeometry(QRect(160, 90, 211, 26));
        P_hide = new QPushButton(groupBox_4);
        P_hide->setObjectName(QStringLiteral("P_hide"));
        P_hide->setGeometry(QRect(367, 86, 51, 31));
        QPalette palette15;
        palette15.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette15.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        palette15.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        P_hide->setPalette(palette15);
        P_hide->setFont(font6);
        P_hide->setCursor(QCursor(Qt::PointingHandCursor));
        P_hide->setFlat(true);
        label_10 = new QLabel(groupBox_4);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(50, 90, 111, 21));
        label_10->setFont(font1);
        P_show = new QPushButton(groupBox_4);
        P_show->setObjectName(QStringLiteral("P_show"));
        P_show->setGeometry(QRect(370, 86, 51, 31));
        QPalette palette16;
        palette16.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette16.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        palette16.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        P_show->setPalette(palette16);
        P_show->setFont(font6);
        P_show->setCursor(QCursor(Qt::PointingHandCursor));
        P_show->setFlat(true);
        P_show->raise();
        plainTextEdit_7->raise();
        label_9->raise();
        N_password->raise();
        N_pass->raise();
        P_hide->raise();
        label_10->raise();

        gridLayout_4->addWidget(groupBox_4, 0, 0, 1, 1);

        forget_pass->setCentralWidget(centralwidget);

        retranslateUi(forget_pass);

        QMetaObject::connectSlotsByName(forget_pass);
    } // setupUi

    void retranslateUi(QMainWindow *forget_pass)
    {
        forget_pass->setWindowTitle(QApplication::translate("forget_pass", "MainWindow", nullptr));
        groupBox->setTitle(QString());
        label->setText(QApplication::translate("forget_pass", "Email:", nullptr));
        label_2->setText(QApplication::translate("forget_pass", "Contact No.:", nullptr));
        plainTextEdit->setPlainText(QApplication::translate("forget_pass", "Please enter your Account type, Email address and contact number to search for your account.", nullptr));
        label_3->setText(QApplication::translate("forget_pass", "Find Your Account:", nullptr));
        label_4->setText(QApplication::translate("forget_pass", "Account Type:", nullptr));
        F_acc->setText(QApplication::translate("forget_pass", "Search->", nullptr));
        label_Email->setText(QApplication::translate("forget_pass", "Invalid!", nullptr));
        label_C_no->setText(QApplication::translate("forget_pass", "Invalid!", nullptr));
        label_A_Info->setText(QApplication::translate("forget_pass", "Invalid!", nullptr));
        groupBox_2->setTitle(QString());
        plainTextEdit_2->setPlainText(QApplication::translate("forget_pass", "Please check your number for a message with your code. Your code is 6 digits long.", nullptr));
        label_5->setText(QApplication::translate("forget_pass", "Enter Security Code:", nullptr));
        S_code->setText(QApplication::translate("forget_pass", "Continue->", nullptr));
        Code->setText(QString());
        plainTextEdit_3->setPlainText(QApplication::translate("forget_pass", "We sent your code to:", nullptr));
        C_num->setPlainText(QApplication::translate("forget_pass", "090078601", nullptr));
        A_C_send->setText(QApplication::translate("forget_pass", "Didn't get code?", nullptr));
        groupBox_3->setTitle(QString());
        label_6->setText(QApplication::translate("forget_pass", "Question Answer:", nullptr));
        plainTextEdit_5->setPlainText(QApplication::translate("forget_pass", "Please enter your Security Question and Answer combination for further verification of your account.", nullptr));
        label_7->setText(QApplication::translate("forget_pass", "Security Question:", nullptr));
        label_8->setText(QApplication::translate("forget_pass", "Enter Security Question:", nullptr));
        S_question->setText(QApplication::translate("forget_pass", "Continue->", nullptr));
        S_que->setPlainText(QApplication::translate("forget_pass", "question??", nullptr));
        groupBox_4->setTitle(QString());
        plainTextEdit_7->setPlainText(QApplication::translate("forget_pass", "A strong password is a combination of letters and punctuation marks. It must be at least 6 characters long.", nullptr));
        label_9->setText(QApplication::translate("forget_pass", "Enter New Password:", nullptr));
        N_password->setText(QApplication::translate("forget_pass", "Confirm->", nullptr));
        N_pass->setText(QString());
        P_hide->setText(QApplication::translate("forget_pass", "Hide", nullptr));
        label_10->setText(QApplication::translate("forget_pass", "New Password:", nullptr));
        P_show->setText(QApplication::translate("forget_pass", "Show", nullptr));
    } // retranslateUi

};

namespace Ui {
    class forget_pass: public Ui_forget_pass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORGET_PASS_H
