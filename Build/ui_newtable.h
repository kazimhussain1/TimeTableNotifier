/********************************************************************************
** Form generated from reading UI file 'newtable.ui'
**
** Created by: Qt User Interface Compiler version 5.11.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NEWTABLE_H
#define UI_NEWTABLE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_NewTable
{
public:
    QLineEdit *lineEdit;
    QPushButton *Submit;

    void setupUi(QWidget *NewTable)
    {
        if (NewTable->objectName().isEmpty())
            NewTable->setObjectName(QStringLiteral("NewTable"));
        NewTable->resize(955, 602);
        lineEdit = new QLineEdit(NewTable);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(60, 30, 113, 21));
        Submit = new QPushButton(NewTable);
        Submit->setObjectName(QStringLiteral("Submit"));
        Submit->setGeometry(QRect(790, 540, 91, 31));
        Submit->setStyleSheet(QStringLiteral("font: 12pt \"MS Shell Dlg 2\";"));

        retranslateUi(NewTable);

        QMetaObject::connectSlotsByName(NewTable);
    } // setupUi

    void retranslateUi(QWidget *NewTable)
    {
        NewTable->setWindowTitle(QApplication::translate("NewTable", "Form", nullptr));
        Submit->setText(QApplication::translate("NewTable", "Submit", nullptr));
    } // retranslateUi

};

namespace Ui {
    class NewTable: public Ui_NewTable {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NEWTABLE_H
