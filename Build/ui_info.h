/********************************************************************************
** Form generated from reading UI file 'info.ui'
**
** Created by: Qt User Interface Compiler version 5.11.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INFO_H
#define UI_INFO_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpinBox>

QT_BEGIN_NAMESPACE

class Ui_Info
{
public:
    QDialogButtonBox *buttonBox;
    QLineEdit *Course;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *CourseNo;
    QLabel *label_3;
    QLineEdit *Name;
    QLabel *label_4;
    QLabel *label_5;
    QSpinBox *TimePeriod;

    void setupUi(QDialog *Info)
    {
        if (Info->objectName().isEmpty())
            Info->setObjectName(QStringLiteral("Info"));
        Info->resize(400, 300);
        buttonBox = new QDialogButtonBox(Info);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(30, 250, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        Course = new QLineEdit(Info);
        Course->setObjectName(QStringLiteral("Course"));
        Course->setGeometry(QRect(140, 70, 161, 21));
        label = new QLabel(Info);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(40, 70, 91, 21));
        label_2 = new QLabel(Info);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(40, 110, 91, 21));
        CourseNo = new QLineEdit(Info);
        CourseNo->setObjectName(QStringLiteral("CourseNo"));
        CourseNo->setGeometry(QRect(140, 110, 161, 21));
        label_3 = new QLabel(Info);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(40, 150, 91, 21));
        Name = new QLineEdit(Info);
        Name->setObjectName(QStringLiteral("Name"));
        Name->setGeometry(QRect(140, 150, 161, 21));
        label_4 = new QLabel(Info);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(40, 190, 91, 21));
        label_5 = new QLabel(Info);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(120, 10, 151, 41));
        TimePeriod = new QSpinBox(Info);
        TimePeriod->setObjectName(QStringLiteral("TimePeriod"));
        TimePeriod->setGeometry(QRect(140, 190, 161, 22));
        TimePeriod->setTabletTracking(false);
        TimePeriod->setMinimum(1);
        TimePeriod->setMaximum(4);
        TimePeriod->setDisplayIntegerBase(10);

        retranslateUi(Info);
        QObject::connect(buttonBox, SIGNAL(accepted()), Info, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), Info, SLOT(reject()));

        QMetaObject::connectSlotsByName(Info);
    } // setupUi

    void retranslateUi(QDialog *Info)
    {
        Info->setWindowTitle(QApplication::translate("Info", "Dialog", nullptr));
        Course->setPlaceholderText(QApplication::translate("Info", "eg: ABC", nullptr));
        label->setText(QApplication::translate("Info", "Course", nullptr));
        label_2->setText(QApplication::translate("Info", "Course #", nullptr));
        CourseNo->setPlaceholderText(QApplication::translate("Info", "eg: 123", nullptr));
        label_3->setText(QApplication::translate("Info", "Professor's Name", nullptr));
        Name->setPlaceholderText(QApplication::translate("Info", "eg: Kazim_Hussain", nullptr));
        label_4->setText(QApplication::translate("Info", "Session Length", nullptr));
        label_5->setText(QApplication::translate("Info", "<html><head/><body><p><span style=\" font-size:20pt;\">Add Session</span></p></body></html>", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Info: public Ui_Info {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INFO_H
