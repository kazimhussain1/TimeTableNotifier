/********************************************************************************
** Form generated from reading UI file 'sign_up.ui'
**
** Created by: Qt User Interface Compiler version 5.11.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SIGN_UP_H
#define UI_SIGN_UP_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_sign_up
{
public:
    QLabel *label_c_n;
    QLabel *label_12;
    QPlainTextEdit *Q_note;
    QLabel *label_A_que;
    QLabel *label_13;
    QLabel *label_11;
    QLabel *label;
    QLabel *label_E_box;
    QLabel *label_3;
    QLabel *label_14;
    QLabel *label_1;
    QLabel *label_A_Info;
    QLabel *label_C_pass;
    QLabel *label_C_email;
    QLabel *label_6;
    QLabel *label_P_box;
    QLabel *label_u_n;
    QLabel *label_2;
    QLabel *label_10;
    QComboBox *A_Info;
    QLineEdit *u_n;
    QLineEdit *c_n;
    QLineEdit *E_box;
    QLineEdit *C_email;
    QLineEdit *P_box;
    QLineEdit *C_pass;
    QLabel *label_term_heading;
    QCheckBox *T_C;
    QComboBox *S_que;
    QLineEdit *S_ans;
    QPushButton *S_up;
    QGroupBox *StudentParams;
    QLabel *label_Seatno;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_Sec;
    QLabel *label_27;
    QLabel *label_Sem;
    QLineEdit *Sec;
    QLineEdit *Seatno;
    QSpinBox *Sem;
    QLabel *label_T_C;
    QLabel *label_S_que;
    QLabel *label_E_match;
    QLabel *label_P_match;

    void setupUi(QWidget *sign_up)
    {
        if (sign_up->objectName().isEmpty())
            sign_up->setObjectName(QStringLiteral("sign_up"));
        sign_up->resize(1000, 700);
        sign_up->setLayoutDirection(Qt::LeftToRight);
        label_c_n = new QLabel(sign_up);
        label_c_n->setObjectName(QStringLiteral("label_c_n"));
        label_c_n->setGeometry(QRect(310, 242, 111, 16));
        QPalette palette;
        QBrush brush(QColor(255, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        QBrush brush1(QColor(120, 120, 120, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        label_c_n->setPalette(palette);
        label_12 = new QLabel(sign_up);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(470, 360, 111, 21));
        QFont font;
        font.setPointSize(9);
        font.setBold(true);
        font.setUnderline(true);
        font.setWeight(75);
        label_12->setFont(font);
        Q_note = new QPlainTextEdit(sign_up);
        Q_note->setObjectName(QStringLiteral("Q_note"));
        Q_note->setEnabled(true);
        Q_note->setGeometry(QRect(474, 300, 501, 41));
        QPalette palette1;
        QBrush brush2(QColor(240, 240, 240, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Base, brush2);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush2);
        Q_note->setPalette(palette1);
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        Q_note->setFont(font1);
        Q_note->setFocusPolicy(Qt::NoFocus);
        Q_note->setFrameShape(QFrame::NoFrame);
        Q_note->setReadOnly(true);
        Q_note->setBackgroundVisible(false);
        label_A_que = new QLabel(sign_up);
        label_A_que->setObjectName(QStringLiteral("label_A_que"));
        label_A_que->setGeometry(QRect(820, 470, 111, 16));
        QPalette palette2;
        palette2.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette2.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        label_A_que->setPalette(palette2);
        label_13 = new QLabel(sign_up);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(150, 465, 131, 31));
        label_13->setFont(font);
        label_11 = new QLabel(sign_up);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(150, 403, 81, 21));
        label_11->setFont(font);
        label = new QLabel(sign_up);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(150, 80, 101, 31));
        label->setFont(font);
        label_E_box = new QLabel(sign_up);
        label_E_box->setObjectName(QStringLiteral("label_E_box"));
        label_E_box->setGeometry(QRect(310, 306, 121, 16));
        QPalette palette3;
        palette3.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette3.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        label_E_box->setPalette(palette3);
        label_3 = new QLabel(sign_up);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(150, 271, 81, 31));
        label_3->setFont(font);
        label_14 = new QLabel(sign_up);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setGeometry(QRect(470, 430, 111, 21));
        label_14->setFont(font);
        label_1 = new QLabel(sign_up);
        label_1->setObjectName(QStringLiteral("label_1"));
        label_1->setGeometry(QRect(150, 145, 81, 31));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_1->sizePolicy().hasHeightForWidth());
        label_1->setSizePolicy(sizePolicy);
        label_1->setFont(font);
        label_A_Info = new QLabel(sign_up);
        label_A_Info->setObjectName(QStringLiteral("label_A_Info"));
        label_A_Info->setGeometry(QRect(310, 115, 121, 16));
        QPalette palette4;
        palette4.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush3(QColor(0, 0, 0, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette4.setBrush(QPalette::Active, QPalette::Text, brush3);
        palette4.setBrush(QPalette::Active, QPalette::ButtonText, brush3);
        palette4.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette4.setBrush(QPalette::Inactive, QPalette::Text, brush3);
        palette4.setBrush(QPalette::Inactive, QPalette::ButtonText, brush3);
        palette4.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        palette4.setBrush(QPalette::Disabled, QPalette::Text, brush1);
        palette4.setBrush(QPalette::Disabled, QPalette::ButtonText, brush1);
        label_A_Info->setPalette(palette4);
        label_C_pass = new QLabel(sign_up);
        label_C_pass->setObjectName(QStringLiteral("label_C_pass"));
        label_C_pass->setGeometry(QRect(310, 500, 111, 16));
        QPalette palette5;
        palette5.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette5.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette5.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        label_C_pass->setPalette(palette5);
        label_C_email = new QLabel(sign_up);
        label_C_email->setObjectName(QStringLiteral("label_C_email"));
        label_C_email->setGeometry(QRect(310, 369, 111, 16));
        QPalette palette6;
        palette6.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette6.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette6.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        label_C_email->setPalette(palette6);
        label_6 = new QLabel(sign_up);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(150, 334, 111, 31));
        label_6->setFont(font);
        label_P_box = new QLabel(sign_up);
        label_P_box->setObjectName(QStringLiteral("label_P_box"));
        label_P_box->setGeometry(QRect(310, 433, 111, 16));
        QPalette palette7;
        palette7.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette7.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette7.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        label_P_box->setPalette(palette7);
        label_u_n = new QLabel(sign_up);
        label_u_n->setObjectName(QStringLiteral("label_u_n"));
        label_u_n->setGeometry(QRect(310, 177, 111, 20));
        QPalette palette8;
        palette8.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette8.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette8.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        label_u_n->setPalette(palette8);
        label_2 = new QLabel(sign_up);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(150, 208, 91, 31));
        label_2->setFont(font);
        label_10 = new QLabel(sign_up);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(90, 10, 221, 81));
        QPalette palette9;
        QBrush brush4(QColor(0, 0, 127, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette9.setBrush(QPalette::Active, QPalette::WindowText, brush4);
        palette9.setBrush(QPalette::Active, QPalette::Text, brush4);
        palette9.setBrush(QPalette::Active, QPalette::ButtonText, brush4);
        palette9.setBrush(QPalette::Inactive, QPalette::WindowText, brush4);
        palette9.setBrush(QPalette::Inactive, QPalette::Text, brush4);
        palette9.setBrush(QPalette::Inactive, QPalette::ButtonText, brush4);
        palette9.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        palette9.setBrush(QPalette::Disabled, QPalette::Text, brush1);
        palette9.setBrush(QPalette::Disabled, QPalette::ButtonText, brush1);
        label_10->setPalette(palette9);
        QFont font2;
        font2.setFamily(QStringLiteral("Palatino Linotype"));
        font2.setPointSize(35);
        font2.setBold(true);
        font2.setItalic(false);
        font2.setUnderline(true);
        font2.setWeight(75);
        label_10->setFont(font2);
        A_Info = new QComboBox(sign_up);
        A_Info->addItem(QString());
        A_Info->addItem(QString());
        A_Info->addItem(QString());
        A_Info->addItem(QString());
        A_Info->addItem(QString());
        A_Info->setObjectName(QStringLiteral("A_Info"));
        A_Info->setGeometry(QRect(150, 110, 151, 26));
        A_Info->setCursor(QCursor(Qt::PointingHandCursor));
        u_n = new QLineEdit(sign_up);
        u_n->setObjectName(QStringLiteral("u_n"));
        u_n->setGeometry(QRect(150, 175, 151, 26));
        c_n = new QLineEdit(sign_up);
        c_n->setObjectName(QStringLiteral("c_n"));
        c_n->setGeometry(QRect(150, 238, 151, 26));
        E_box = new QLineEdit(sign_up);
        E_box->setObjectName(QStringLiteral("E_box"));
        E_box->setGeometry(QRect(150, 301, 151, 26));
        C_email = new QLineEdit(sign_up);
        C_email->setObjectName(QStringLiteral("C_email"));
        C_email->setGeometry(QRect(150, 365, 151, 26));
        P_box = new QLineEdit(sign_up);
        P_box->setObjectName(QStringLiteral("P_box"));
        P_box->setGeometry(QRect(150, 429, 151, 26));
        P_box->setEchoMode(QLineEdit::Password);
        C_pass = new QLineEdit(sign_up);
        C_pass->setObjectName(QStringLiteral("C_pass"));
        C_pass->setGeometry(QRect(150, 496, 151, 26));
        C_pass->setEchoMode(QLineEdit::Password);
        label_term_heading = new QLabel(sign_up);
        label_term_heading->setObjectName(QStringLiteral("label_term_heading"));
        label_term_heading->setGeometry(QRect(450, 570, 201, 31));
        QFont font3;
        font3.setPointSize(14);
        font3.setBold(true);
        font3.setUnderline(true);
        font3.setWeight(75);
        label_term_heading->setFont(font3);
        T_C = new QCheckBox(sign_up);
        T_C->setObjectName(QStringLiteral("T_C"));
        T_C->setGeometry(QRect(450, 630, 121, 21));
        QFont font4;
        font4.setPointSize(14);
        font4.setBold(true);
        font4.setWeight(75);
        T_C->setFont(font4);
        T_C->setCursor(QCursor(Qt::PointingHandCursor));
        S_que = new QComboBox(sign_up);
        S_que->addItem(QString());
        S_que->addItem(QString());
        S_que->addItem(QString());
        S_que->addItem(QString());
        S_que->addItem(QString());
        S_que->addItem(QString());
        S_que->setObjectName(QStringLiteral("S_que"));
        S_que->setGeometry(QRect(470, 390, 341, 26));
        S_que->setCursor(QCursor(Qt::PointingHandCursor));
        S_ans = new QLineEdit(sign_up);
        S_ans->setObjectName(QStringLiteral("S_ans"));
        S_ans->setGeometry(QRect(470, 460, 341, 26));
        S_up = new QPushButton(sign_up);
        S_up->setObjectName(QStringLiteral("S_up"));
        S_up->setGeometry(QRect(880, 630, 91, 41));
        S_up->setCursor(QCursor(Qt::PointingHandCursor));
        StudentParams = new QGroupBox(sign_up);
        StudentParams->setObjectName(QStringLiteral("StudentParams"));
        StudentParams->setGeometry(QRect(490, 50, 291, 221));
        label_Seatno = new QLabel(StudentParams);
        label_Seatno->setObjectName(QStringLiteral("label_Seatno"));
        label_Seatno->setGeometry(QRect(170, 190, 101, 16));
        QPalette palette10;
        palette10.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette10.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette10.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        label_Seatno->setPalette(palette10);
        label_8 = new QLabel(StudentParams);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(10, 90, 111, 31));
        label_8->setFont(font);
        label_9 = new QLabel(StudentParams);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(10, 29, 61, 31));
        label_9->setFont(font);
        label_Sec = new QLabel(StudentParams);
        label_Sec->setObjectName(QStringLiteral("label_Sec"));
        label_Sec->setGeometry(QRect(170, 63, 111, 16));
        QPalette palette11;
        palette11.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette11.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette11.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        label_Sec->setPalette(palette11);
        label_27 = new QLabel(StudentParams);
        label_27->setObjectName(QStringLiteral("label_27"));
        label_27->setGeometry(QRect(10, 170, 61, 16));
        label_27->setFont(font);
        label_Sem = new QLabel(StudentParams);
        label_Sem->setObjectName(QStringLiteral("label_Sem"));
        label_Sem->setGeometry(QRect(170, 130, 111, 16));
        QPalette palette12;
        palette12.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette12.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette12.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        label_Sem->setPalette(palette12);
        Sec = new QLineEdit(StudentParams);
        Sec->setObjectName(QStringLiteral("Sec"));
        Sec->setGeometry(QRect(10, 60, 151, 26));
        Seatno = new QLineEdit(StudentParams);
        Seatno->setObjectName(QStringLiteral("Seatno"));
        Seatno->setGeometry(QRect(10, 190, 151, 26));
        Sem = new QSpinBox(StudentParams);
        Sem->setObjectName(QStringLiteral("Sem"));
        Sem->setGeometry(QRect(10, 120, 151, 26));
        Sem->setCursor(QCursor(Qt::PointingHandCursor));
        Sem->setMouseTracking(true);
        Sem->setTabletTracking(true);
        Sem->setWrapping(false);
        Sem->setFrame(true);
        Sem->setMinimum(0);
        Sem->setMaximum(8);
        Sem->setValue(0);
        label_T_C = new QLabel(sign_up);
        label_T_C->setObjectName(QStringLiteral("label_T_C"));
        label_T_C->setGeometry(QRect(570, 630, 101, 16));
        QPalette palette13;
        palette13.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette13.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette13.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        label_T_C->setPalette(palette13);
        label_S_que = new QLabel(sign_up);
        label_S_que->setObjectName(QStringLiteral("label_S_que"));
        label_S_que->setGeometry(QRect(820, 400, 161, 16));
        QPalette palette14;
        palette14.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette14.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette14.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        label_S_que->setPalette(palette14);
        label_E_match = new QLabel(sign_up);
        label_E_match->setObjectName(QStringLiteral("label_E_match"));
        label_E_match->setGeometry(QRect(310, 370, 111, 16));
        QPalette palette15;
        palette15.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette15.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette15.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        label_E_match->setPalette(palette15);
        label_P_match = new QLabel(sign_up);
        label_P_match->setObjectName(QStringLiteral("label_P_match"));
        label_P_match->setGeometry(QRect(310, 500, 121, 16));
        QPalette palette16;
        palette16.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette16.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette16.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        label_P_match->setPalette(palette16);
        Q_note->raise();
        label_A_que->raise();
        label_13->raise();
        label_11->raise();
        label_14->raise();
        label_C_pass->raise();
        label_C_email->raise();
        label_6->raise();
        label_P_box->raise();
        label_A_Info->raise();
        label_10->raise();
        label_2->raise();
        label_c_n->raise();
        label_u_n->raise();
        label->raise();
        label_1->raise();
        label_3->raise();
        label_E_box->raise();
        label_12->raise();
        A_Info->raise();
        u_n->raise();
        c_n->raise();
        E_box->raise();
        C_email->raise();
        P_box->raise();
        C_pass->raise();
        label_term_heading->raise();
        T_C->raise();
        S_que->raise();
        S_ans->raise();
        S_up->raise();
        StudentParams->raise();
        label_T_C->raise();
        label_S_que->raise();
        label_E_match->raise();
        label_P_match->raise();

        retranslateUi(sign_up);

        QMetaObject::connectSlotsByName(sign_up);
    } // setupUi

    void retranslateUi(QWidget *sign_up)
    {
        sign_up->setWindowTitle(QApplication::translate("sign_up", "Form", nullptr));
        label_c_n->setText(QApplication::translate("sign_up", "This field is required.", nullptr));
        label_12->setText(QApplication::translate("sign_up", "Secret Question:", nullptr));
        Q_note->setPlainText(QApplication::translate("sign_up", "Please enter a secret question and answer combination. You will be asked for this combination if you ever forget your password and need to reset it.", nullptr));
        label_A_que->setText(QApplication::translate("sign_up", "This field is required.", nullptr));
        label_13->setText(QApplication::translate("sign_up", "Confirm Password:", nullptr));
        label_11->setText(QApplication::translate("sign_up", "Password:", nullptr));
        label->setText(QApplication::translate("sign_up", "Account Type:", nullptr));
        label_E_box->setText(QApplication::translate("sign_up", "This field is required.", nullptr));
        label_3->setText(QApplication::translate("sign_up", "Email:", nullptr));
        label_14->setText(QApplication::translate("sign_up", "Answer Question:", nullptr));
        label_1->setText(QApplication::translate("sign_up", "User Name:", nullptr));
        label_A_Info->setText(QApplication::translate("sign_up", "This field is required.", nullptr));
        label_C_pass->setText(QApplication::translate("sign_up", "This field is required.", nullptr));
        label_C_email->setText(QApplication::translate("sign_up", "This field is required.", nullptr));
        label_6->setText(QApplication::translate("sign_up", "Confirm Email:", nullptr));
        label_P_box->setText(QApplication::translate("sign_up", "This field is required.", nullptr));
        label_u_n->setText(QApplication::translate("sign_up", "This field is required.", nullptr));
        label_2->setText(QApplication::translate("sign_up", "Contact No.:", nullptr));
        label_10->setText(QApplication::translate("sign_up", "Signup:", nullptr));
        A_Info->setItemText(0, QApplication::translate("sign_up", "<<Select Here>>", nullptr));
        A_Info->setItemText(1, QApplication::translate("sign_up", "Course Supervisor", nullptr));
        A_Info->setItemText(2, QApplication::translate("sign_up", "Professor", nullptr));
        A_Info->setItemText(3, QApplication::translate("sign_up", "Class Representative", nullptr));
        A_Info->setItemText(4, QApplication::translate("sign_up", "Student", nullptr));

        label_term_heading->setText(QApplication::translate("sign_up", "Terms & Conditions:", nullptr));
        T_C->setText(QApplication::translate("sign_up", "I AGREE..", nullptr));
        S_que->setItemText(0, QApplication::translate("sign_up", "<<Select Here>>", nullptr));
        S_que->setItemText(1, QApplication::translate("sign_up", "What is the name of your favorite pet?", nullptr));
        S_que->setItemText(2, QApplication::translate("sign_up", "What was your favorite place to visit as a child?", nullptr));
        S_que->setItemText(3, QApplication::translate("sign_up", "Which phone number do you remember most from your childhood?", nullptr));
        S_que->setItemText(4, QApplication::translate("sign_up", "What is your favorite movie?", nullptr));
        S_que->setItemText(5, QApplication::translate("sign_up", "Which is your favorite web browser?", nullptr));

        S_up->setText(QApplication::translate("sign_up", "SIGNUP", nullptr));
        StudentParams->setTitle(QString());
        label_Seatno->setText(QApplication::translate("sign_up", "This field is required.", nullptr));
        label_8->setText(QApplication::translate("sign_up", "Semester:", nullptr));
        label_9->setText(QApplication::translate("sign_up", "Section:", nullptr));
        label_Sec->setText(QApplication::translate("sign_up", "This field is required.", nullptr));
        label_27->setText(QApplication::translate("sign_up", "Seat No.:", nullptr));
        label_Sem->setText(QApplication::translate("sign_up", "This field is required.", nullptr));
        label_T_C->setText(QApplication::translate("sign_up", "This field is required.", nullptr));
        label_S_que->setText(QApplication::translate("sign_up", "<html><head/><body><p>Please Select a valid selection</p></body></html>", nullptr));
        label_E_match->setText(QApplication::translate("sign_up", "<html><head/><body><p>Emails do not match.</p></body></html>", nullptr));
        label_P_match->setText(QApplication::translate("sign_up", "<html><head/><body><p>Passwords do not match.</p></body></html>", nullptr));
    } // retranslateUi

};

namespace Ui {
    class sign_up: public Ui_sign_up {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SIGN_UP_H
