/********************************************************************************
** Form generated from reading UI file 'loadscreen.ui'
**
** Created by: Qt User Interface Compiler version 5.11.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOADSCREEN_H
#define UI_LOADSCREEN_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LoadScreen
{
public:
    QProgressBar *progressBar;

    void setupUi(QWidget *LoadScreen)
    {
        if (LoadScreen->objectName().isEmpty())
            LoadScreen->setObjectName(QStringLiteral("LoadScreen"));
        LoadScreen->resize(1000, 650);
        progressBar = new QProgressBar(LoadScreen);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setGeometry(QRect(0, 590, 1041, 41));
        progressBar->setValue(0);

        retranslateUi(LoadScreen);

        QMetaObject::connectSlotsByName(LoadScreen);
    } // setupUi

    void retranslateUi(QWidget *LoadScreen)
    {
        LoadScreen->setWindowTitle(QApplication::translate("LoadScreen", "Time Table Notifier", nullptr));
    } // retranslateUi

};

namespace Ui {
    class LoadScreen: public Ui_LoadScreen {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOADSCREEN_H
