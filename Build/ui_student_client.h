/********************************************************************************
** Form generated from reading UI file 'student_client.ui'
**
** Created by: Qt User Interface Compiler version 5.11.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_STUDENT_CLIENT_H
#define UI_STUDENT_CLIENT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_StudentClient
{
public:
    QWidget *centralwidget;
    QGroupBox *gridGroupBox_2;
    QGridLayout *gridLayout_4;
    QGroupBox *groupBox_4;
    QGroupBox *gridGroupBox;
    QGridLayout *gridLayout;
    QGroupBox *groupBox;
    QLineEdit *u_n_3;
    QLabel *label_7;
    QLineEdit *c_n_3;
    QLineEdit *E_box_3;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QLineEdit *E_box_4;
    QLabel *label_11;
    QLineEdit *E_box_5;
    QLabel *label_12;
    QLineEdit *E_box_6;
    QLabel *label_13;
    QPushButton *P_edit;
    QLabel *label_profile;
    QLabel *label_setting;
    QLabel *label_P_notes;
    QGroupBox *gridGroupBox1;
    QGridLayout *gridLayout_2;
    QGroupBox *groupBox_2;
    QLineEdit *u_n_2;
    QLabel *label_4;
    QLineEdit *c_n_2;
    QLineEdit *E_box_2;
    QLabel *C_pass1;
    QLineEdit *P_box;
    QLineEdit *C_pass;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_14;
    QPushButton *E_u_n;
    QLabel *l_Theme;
    QPushButton *E_c_n;
    QPushButton *E_email;
    QPushButton *E_pass;
    QPushButton *E_sem;
    QLineEdit *Semester;
    QLabel *Sem;
    QPushButton *E_theme;
    QLineEdit *Theme;
    QGroupBox *gridGroupBox2;
    QGridLayout *gridLayout_6;
    QGroupBox *groupBox_6;
    QPushButton *add_2;
    QPlainTextEdit *plainTextEdit_2;
    QGroupBox *groupBox_3;
    QLabel *label;
    QTableWidget *tableWidget;
    QGroupBox *gridGroupBox4;
    QGridLayout *gridLayout_5;
    QGroupBox *groupBox_5;
    QPushButton *P_notes;
    QPushButton *Profile;
    QPushButton *Setting;
    QPushButton *T_table;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *StudentClient)
    {
        if (StudentClient->objectName().isEmpty())
            StudentClient->setObjectName(QStringLiteral("StudentClient"));
        StudentClient->resize(1200, 700);
        centralwidget = new QWidget(StudentClient);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        gridGroupBox_2 = new QGroupBox(centralwidget);
        gridGroupBox_2->setObjectName(QStringLiteral("gridGroupBox_2"));
        gridGroupBox_2->setGeometry(QRect(180, -20, 1021, 701));
        gridLayout_4 = new QGridLayout(gridGroupBox_2);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        groupBox_4 = new QGroupBox(gridGroupBox_2);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        gridGroupBox = new QGroupBox(groupBox_4);
        gridGroupBox->setObjectName(QStringLiteral("gridGroupBox"));
        gridGroupBox->setGeometry(QRect(230, 190, 551, 311));
        gridLayout = new QGridLayout(gridGroupBox);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        groupBox = new QGroupBox(gridGroupBox);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        u_n_3 = new QLineEdit(groupBox);
        u_n_3->setObjectName(QStringLiteral("u_n_3"));
        u_n_3->setGeometry(QRect(40, 100, 191, 26));
        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(116, 70, 41, 31));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_7->sizePolicy().hasHeightForWidth());
        label_7->setSizePolicy(sizePolicy);
        QFont font;
        font.setPointSize(9);
        font.setBold(true);
        font.setUnderline(true);
        font.setWeight(75);
        label_7->setFont(font);
        c_n_3 = new QLineEdit(groupBox);
        c_n_3->setObjectName(QStringLiteral("c_n_3"));
        c_n_3->setGeometry(QRect(40, 163, 191, 26));
        E_box_3 = new QLineEdit(groupBox);
        E_box_3->setObjectName(QStringLiteral("E_box_3"));
        E_box_3->setGeometry(QRect(40, 226, 191, 26));
        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(97, 133, 91, 31));
        label_8->setFont(font);
        label_9 = new QLabel(groupBox);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(117, 196, 41, 31));
        label_9->setFont(font);
        label_10 = new QLabel(groupBox);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(220, 20, 101, 31));
        QFont font1;
        font1.setFamily(QStringLiteral("Palatino Linotype"));
        font1.setPointSize(19);
        font1.setBold(true);
        font1.setUnderline(true);
        font1.setWeight(75);
        label_10->setFont(font1);
        E_box_4 = new QLineEdit(groupBox);
        E_box_4->setObjectName(QStringLiteral("E_box_4"));
        E_box_4->setGeometry(QRect(300, 100, 191, 26));
        label_11 = new QLabel(groupBox);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(370, 69, 61, 31));
        label_11->setFont(font);
        E_box_5 = new QLineEdit(groupBox);
        E_box_5->setObjectName(QStringLiteral("E_box_5"));
        E_box_5->setGeometry(QRect(300, 163, 191, 26));
        label_12 = new QLabel(groupBox);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(363, 133, 71, 31));
        label_12->setFont(font);
        E_box_6 = new QLineEdit(groupBox);
        E_box_6->setObjectName(QStringLiteral("E_box_6"));
        E_box_6->setGeometry(QRect(300, 226, 191, 26));
        label_13 = new QLabel(groupBox);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(368, 196, 61, 31));
        label_13->setFont(font);
        P_edit = new QPushButton(groupBox);
        P_edit->setObjectName(QStringLiteral("P_edit"));
        P_edit->setGeometry(QRect(450, 10, 75, 23));
        QPalette palette;
        QBrush brush(QColor(0, 0, 127, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        QBrush brush1(QColor(120, 120, 120, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush1);
        P_edit->setPalette(palette);
        QFont font2;
        font2.setPointSize(9);
        font2.setUnderline(true);
        P_edit->setFont(font2);
        P_edit->setFlat(true);

        gridLayout->addWidget(groupBox, 0, 1, 1, 1);

        label_profile = new QLabel(groupBox_4);
        label_profile->setObjectName(QStringLiteral("label_profile"));
        label_profile->setGeometry(QRect(448, 140, 121, 41));
        QFont font3;
        font3.setFamily(QStringLiteral("Palatino Linotype"));
        font3.setPointSize(27);
        font3.setBold(true);
        font3.setUnderline(true);
        font3.setWeight(75);
        label_profile->setFont(font3);
        label_setting = new QLabel(groupBox_4);
        label_setting->setObjectName(QStringLiteral("label_setting"));
        label_setting->setGeometry(QRect(438, 100, 181, 51));
        label_setting->setFont(font3);
        label_P_notes = new QLabel(groupBox_4);
        label_P_notes->setObjectName(QStringLiteral("label_P_notes"));
        label_P_notes->setGeometry(QRect(380, 80, 261, 51));
        label_P_notes->setFont(font3);
        gridGroupBox1 = new QGroupBox(groupBox_4);
        gridGroupBox1->setObjectName(QStringLiteral("gridGroupBox1"));
        gridGroupBox1->setGeometry(QRect(285, 154, 441, 381));
        gridLayout_2 = new QGridLayout(gridGroupBox1);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        groupBox_2 = new QGroupBox(gridGroupBox1);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        u_n_2 = new QLineEdit(groupBox_2);
        u_n_2->setObjectName(QStringLiteral("u_n_2"));
        u_n_2->setGeometry(QRect(150, 50, 151, 26));
        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(60, 50, 81, 31));
        sizePolicy.setHeightForWidth(label_4->sizePolicy().hasHeightForWidth());
        label_4->setSizePolicy(sizePolicy);
        label_4->setFont(font);
        c_n_2 = new QLineEdit(groupBox_2);
        c_n_2->setObjectName(QStringLiteral("c_n_2"));
        c_n_2->setGeometry(QRect(150, 90, 151, 26));
        E_box_2 = new QLineEdit(groupBox_2);
        E_box_2->setObjectName(QStringLiteral("E_box_2"));
        E_box_2->setGeometry(QRect(150, 130, 151, 26));
        C_pass1 = new QLabel(groupBox_2);
        C_pass1->setObjectName(QStringLiteral("C_pass1"));
        C_pass1->setGeometry(QRect(60, 207, 131, 31));
        C_pass1->setFont(font);
        P_box = new QLineEdit(groupBox_2);
        P_box->setObjectName(QStringLiteral("P_box"));
        P_box->setGeometry(QRect(150, 170, 151, 26));
        P_box->setEchoMode(QLineEdit::Password);
        C_pass = new QLineEdit(groupBox_2);
        C_pass->setObjectName(QStringLiteral("C_pass"));
        C_pass->setGeometry(QRect(190, 210, 151, 26));
        C_pass->setEchoMode(QLineEdit::Password);
        label_5 = new QLabel(groupBox_2);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(60, 90, 91, 31));
        label_5->setFont(font);
        label_6 = new QLabel(groupBox_2);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(60, 130, 81, 31));
        label_6->setFont(font);
        label_14 = new QLabel(groupBox_2);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setGeometry(QRect(60, 170, 81, 21));
        label_14->setFont(font);
        E_u_n = new QPushButton(groupBox_2);
        E_u_n->setObjectName(QStringLiteral("E_u_n"));
        E_u_n->setGeometry(QRect(310, 50, 41, 21));
        QPalette palette1;
        QBrush brush2(QColor(0, 0, 0, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush2);
        QBrush brush3(QColor(1, 14, 200, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::ButtonText, brush3);
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush2);
        palette1.setBrush(QPalette::Inactive, QPalette::ButtonText, brush3);
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::ButtonText, brush1);
        E_u_n->setPalette(palette1);
        QFont font4;
        font4.setUnderline(true);
        E_u_n->setFont(font4);
        E_u_n->setCursor(QCursor(Qt::PointingHandCursor));
        E_u_n->setFlat(true);
        l_Theme = new QLabel(groupBox_2);
        l_Theme->setObjectName(QStringLiteral("l_Theme"));
        l_Theme->setGeometry(QRect(60, 245, 51, 31));
        l_Theme->setFont(font);
        E_c_n = new QPushButton(groupBox_2);
        E_c_n->setObjectName(QStringLiteral("E_c_n"));
        E_c_n->setGeometry(QRect(310, 90, 41, 21));
        QPalette palette2;
        palette2.setBrush(QPalette::Active, QPalette::WindowText, brush2);
        palette2.setBrush(QPalette::Active, QPalette::ButtonText, brush3);
        palette2.setBrush(QPalette::Inactive, QPalette::WindowText, brush2);
        palette2.setBrush(QPalette::Inactive, QPalette::ButtonText, brush3);
        palette2.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        palette2.setBrush(QPalette::Disabled, QPalette::ButtonText, brush1);
        E_c_n->setPalette(palette2);
        E_c_n->setFont(font4);
        E_c_n->setCursor(QCursor(Qt::PointingHandCursor));
        E_c_n->setFlat(true);
        E_email = new QPushButton(groupBox_2);
        E_email->setObjectName(QStringLiteral("E_email"));
        E_email->setGeometry(QRect(310, 130, 41, 21));
        QPalette palette3;
        palette3.setBrush(QPalette::Active, QPalette::WindowText, brush2);
        palette3.setBrush(QPalette::Active, QPalette::ButtonText, brush3);
        palette3.setBrush(QPalette::Inactive, QPalette::WindowText, brush2);
        palette3.setBrush(QPalette::Inactive, QPalette::ButtonText, brush3);
        palette3.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::ButtonText, brush1);
        E_email->setPalette(palette3);
        E_email->setFont(font4);
        E_email->setCursor(QCursor(Qt::PointingHandCursor));
        E_email->setFlat(true);
        E_pass = new QPushButton(groupBox_2);
        E_pass->setObjectName(QStringLiteral("E_pass"));
        E_pass->setGeometry(QRect(310, 170, 41, 21));
        QPalette palette4;
        palette4.setBrush(QPalette::Active, QPalette::WindowText, brush2);
        palette4.setBrush(QPalette::Active, QPalette::ButtonText, brush3);
        palette4.setBrush(QPalette::Inactive, QPalette::WindowText, brush2);
        palette4.setBrush(QPalette::Inactive, QPalette::ButtonText, brush3);
        palette4.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        palette4.setBrush(QPalette::Disabled, QPalette::ButtonText, brush1);
        E_pass->setPalette(palette4);
        E_pass->setFont(font4);
        E_pass->setCursor(QCursor(Qt::PointingHandCursor));
        E_pass->setFlat(true);
        E_sem = new QPushButton(groupBox_2);
        E_sem->setObjectName(QStringLiteral("E_sem"));
        E_sem->setGeometry(QRect(310, 210, 41, 21));
        QPalette palette5;
        palette5.setBrush(QPalette::Active, QPalette::WindowText, brush2);
        palette5.setBrush(QPalette::Active, QPalette::ButtonText, brush3);
        palette5.setBrush(QPalette::Inactive, QPalette::WindowText, brush2);
        palette5.setBrush(QPalette::Inactive, QPalette::ButtonText, brush3);
        palette5.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        palette5.setBrush(QPalette::Disabled, QPalette::ButtonText, brush1);
        E_sem->setPalette(palette5);
        E_sem->setFont(font4);
        E_sem->setCursor(QCursor(Qt::PointingHandCursor));
        E_sem->setFlat(true);
        Semester = new QLineEdit(groupBox_2);
        Semester->setObjectName(QStringLiteral("Semester"));
        Semester->setGeometry(QRect(150, 210, 151, 26));
        Sem = new QLabel(groupBox_2);
        Sem->setObjectName(QStringLiteral("Sem"));
        Sem->setGeometry(QRect(60, 207, 81, 31));
        sizePolicy.setHeightForWidth(Sem->sizePolicy().hasHeightForWidth());
        Sem->setSizePolicy(sizePolicy);
        Sem->setFont(font);
        E_theme = new QPushButton(groupBox_2);
        E_theme->setObjectName(QStringLiteral("E_theme"));
        E_theme->setGeometry(QRect(310, 250, 39, 21));
        QPalette palette6;
        palette6.setBrush(QPalette::Active, QPalette::WindowText, brush2);
        palette6.setBrush(QPalette::Active, QPalette::ButtonText, brush3);
        palette6.setBrush(QPalette::Inactive, QPalette::WindowText, brush2);
        palette6.setBrush(QPalette::Inactive, QPalette::ButtonText, brush3);
        palette6.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        palette6.setBrush(QPalette::Disabled, QPalette::ButtonText, brush1);
        E_theme->setPalette(palette6);
        E_theme->setFont(font4);
        E_theme->setCursor(QCursor(Qt::PointingHandCursor));
        E_theme->setFlat(true);
        Theme = new QLineEdit(groupBox_2);
        Theme->setObjectName(QStringLiteral("Theme"));
        Theme->setGeometry(QRect(150, 250, 151, 26));

        gridLayout_2->addWidget(groupBox_2, 0, 0, 1, 1);

        gridGroupBox2 = new QGroupBox(groupBox_4);
        gridGroupBox2->setObjectName(QStringLiteral("gridGroupBox2"));
        gridGroupBox2->setGeometry(QRect(120, 134, 771, 421));
        gridLayout_6 = new QGridLayout(gridGroupBox2);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        groupBox_6 = new QGroupBox(gridGroupBox2);
        groupBox_6->setObjectName(QStringLiteral("groupBox_6"));
        add_2 = new QPushButton(groupBox_6);
        add_2->setObjectName(QStringLiteral("add_2"));
        add_2->setGeometry(QRect(340, 10, 75, 23));
        QPalette palette7;
        palette7.setBrush(QPalette::Active, QPalette::ButtonText, brush3);
        palette7.setBrush(QPalette::Inactive, QPalette::ButtonText, brush3);
        palette7.setBrush(QPalette::Disabled, QPalette::ButtonText, brush1);
        add_2->setPalette(palette7);
        QFont font5;
        font5.setPointSize(10);
        font5.setBold(false);
        font5.setUnderline(true);
        font5.setWeight(50);
        add_2->setFont(font5);
        add_2->setCursor(QCursor(Qt::PointingHandCursor));
        add_2->setFlat(true);
        plainTextEdit_2 = new QPlainTextEdit(groupBox_6);
        plainTextEdit_2->setObjectName(QStringLiteral("plainTextEdit_2"));
        plainTextEdit_2->setGeometry(QRect(10, 10, 731, 31));
        plainTextEdit_2->viewport()->setProperty("cursor", QVariant(QCursor(Qt::IBeamCursor)));
        plainTextEdit_2->raise();
        add_2->raise();

        gridLayout_6->addWidget(groupBox_6, 0, 0, 1, 1);

        groupBox_3 = new QGroupBox(groupBox_4);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setGeometry(QRect(0, 20, 991, 581));
        groupBox_3->setStyleSheet(QStringLiteral(""));
        label = new QLabel(groupBox_3);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(410, 20, 121, 31));
        tableWidget = new QTableWidget(groupBox_3);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));
        tableWidget->setGeometry(QRect(50, 70, 391, 191));

        gridLayout_4->addWidget(groupBox_4, 0, 0, 1, 1);

        gridGroupBox4 = new QGroupBox(centralwidget);
        gridGroupBox4->setObjectName(QStringLiteral("gridGroupBox4"));
        gridGroupBox4->setGeometry(QRect(1, -20, 181, 701));
        gridLayout_5 = new QGridLayout(gridGroupBox4);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        groupBox_5 = new QGroupBox(gridGroupBox4);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        P_notes = new QPushButton(groupBox_5);
        P_notes->setObjectName(QStringLiteral("P_notes"));
        P_notes->setGeometry(QRect(8, 170, 141, 51));
        QFont font6;
        font6.setPointSize(12);
        font6.setBold(true);
        font6.setWeight(75);
        P_notes->setFont(font6);
        P_notes->setCursor(QCursor(Qt::PointingHandCursor));
        Profile = new QPushButton(groupBox_5);
        Profile->setObjectName(QStringLiteral("Profile"));
        Profile->setGeometry(QRect(8, 20, 141, 51));
        Profile->setFont(font6);
        Profile->setCursor(QCursor(Qt::PointingHandCursor));
        Setting = new QPushButton(groupBox_5);
        Setting->setObjectName(QStringLiteral("Setting"));
        Setting->setGeometry(QRect(8, 120, 141, 51));
        Setting->setFont(font6);
        Setting->setCursor(QCursor(Qt::PointingHandCursor));
        T_table = new QPushButton(groupBox_5);
        T_table->setObjectName(QStringLiteral("T_table"));
        T_table->setGeometry(QRect(8, 70, 141, 51));
        T_table->setFont(font6);
        T_table->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_5->addWidget(groupBox_5, 0, 0, 1, 1);

        StudentClient->setCentralWidget(centralwidget);
        menubar = new QMenuBar(StudentClient);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 1200, 20));
        StudentClient->setMenuBar(menubar);
        statusbar = new QStatusBar(StudentClient);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        StudentClient->setStatusBar(statusbar);

        retranslateUi(StudentClient);

        QMetaObject::connectSlotsByName(StudentClient);
    } // setupUi

    void retranslateUi(QMainWindow *StudentClient)
    {
        StudentClient->setWindowTitle(QApplication::translate("StudentClient", "MainWindow", nullptr));
        groupBox_4->setTitle(QString());
        label_7->setText(QApplication::translate("StudentClient", "Name:", nullptr));
        label_8->setText(QApplication::translate("StudentClient", "Contact No.:", nullptr));
        label_9->setText(QApplication::translate("StudentClient", "Email:", nullptr));
        label_10->setText(QApplication::translate("StudentClient", "Student", nullptr));
        label_11->setText(QApplication::translate("StudentClient", "Section:", nullptr));
        label_12->setText(QApplication::translate("StudentClient", "Semester:", nullptr));
        label_13->setText(QApplication::translate("StudentClient", "Seat No.:", nullptr));
        P_edit->setText(QApplication::translate("StudentClient", "Edit", nullptr));
        label_profile->setText(QApplication::translate("StudentClient", "Profile:", nullptr));
        label_setting->setText(QApplication::translate("StudentClient", "Settings:", nullptr));
        label_P_notes->setText(QApplication::translate("StudentClient", "Personal Notes:", nullptr));
        groupBox_2->setTitle(QString());
        label_4->setText(QApplication::translate("StudentClient", "User Name:", nullptr));
        C_pass1->setText(QApplication::translate("StudentClient", "Confirm Password:", nullptr));
        label_5->setText(QApplication::translate("StudentClient", "Contact No.:", nullptr));
        label_6->setText(QApplication::translate("StudentClient", "Email:", nullptr));
        label_14->setText(QApplication::translate("StudentClient", "Password:", nullptr));
        E_u_n->setText(QApplication::translate("StudentClient", "Edit", nullptr));
        l_Theme->setText(QApplication::translate("StudentClient", "Theme:", nullptr));
        E_c_n->setText(QApplication::translate("StudentClient", "Edit", nullptr));
        E_email->setText(QApplication::translate("StudentClient", "Edit", nullptr));
        E_pass->setText(QApplication::translate("StudentClient", "Edit", nullptr));
        E_sem->setText(QApplication::translate("StudentClient", "Edit", nullptr));
        Sem->setText(QApplication::translate("StudentClient", "Semester:", nullptr));
        E_theme->setText(QApplication::translate("StudentClient", "Edit", nullptr));
        groupBox_6->setTitle(QString());
        add_2->setText(QApplication::translate("StudentClient", "Add", nullptr));
        groupBox_3->setTitle(QString());
        label->setText(QApplication::translate("StudentClient", "<html><head/><body><p><span style=\" font-size:16pt; font-weight:600;\">Time Table</span></p></body></html>", nullptr));
        groupBox_5->setTitle(QString());
        P_notes->setText(QApplication::translate("StudentClient", "Personal Notes", nullptr));
        Profile->setText(QApplication::translate("StudentClient", "Profile", nullptr));
        Setting->setText(QApplication::translate("StudentClient", "Settings", nullptr));
        T_table->setText(QApplication::translate("StudentClient", "TimeTable", nullptr));
    } // retranslateUi

};

namespace Ui {
    class StudentClient: public Ui_StudentClient {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_STUDENT_CLIENT_H
