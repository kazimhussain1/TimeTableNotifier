/****************************************************************************
** Meta object code from reading C++ file 'time_table_manager.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../TimeTableNotifier/time_table_manager.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'time_table_manager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_time_table_manager_t {
    QByteArrayData data[9];
    char stringdata0[149];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_time_table_manager_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_time_table_manager_t qt_meta_stringdata_time_table_manager = {
    {
QT_MOC_LITERAL(0, 0, 18), // "time_table_manager"
QT_MOC_LITERAL(1, 19, 18), // "on_Profile_clicked"
QT_MOC_LITERAL(2, 38, 0), // ""
QT_MOC_LITERAL(3, 39, 18), // "on_Setting_clicked"
QT_MOC_LITERAL(4, 58, 18), // "on_P_notes_clicked"
QT_MOC_LITERAL(5, 77, 14), // "on_add_clicked"
QT_MOC_LITERAL(6, 92, 17), // "on_E_pass_clicked"
QT_MOC_LITERAL(7, 110, 18), // "on_C_table_clicked"
QT_MOC_LITERAL(8, 129, 19) // "on_T_create_clicked"

    },
    "time_table_manager\0on_Profile_clicked\0"
    "\0on_Setting_clicked\0on_P_notes_clicked\0"
    "on_add_clicked\0on_E_pass_clicked\0"
    "on_C_table_clicked\0on_T_create_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_time_table_manager[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   49,    2, 0x08 /* Private */,
       3,    0,   50,    2, 0x08 /* Private */,
       4,    0,   51,    2, 0x08 /* Private */,
       5,    0,   52,    2, 0x08 /* Private */,
       6,    0,   53,    2, 0x08 /* Private */,
       7,    0,   54,    2, 0x08 /* Private */,
       8,    0,   55,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void time_table_manager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        time_table_manager *_t = static_cast<time_table_manager *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_Profile_clicked(); break;
        case 1: _t->on_Setting_clicked(); break;
        case 2: _t->on_P_notes_clicked(); break;
        case 3: _t->on_add_clicked(); break;
        case 4: _t->on_E_pass_clicked(); break;
        case 5: _t->on_C_table_clicked(); break;
        case 6: _t->on_T_create_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject time_table_manager::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_time_table_manager.data,
      qt_meta_data_time_table_manager,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *time_table_manager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *time_table_manager::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_time_table_manager.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int time_table_manager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
