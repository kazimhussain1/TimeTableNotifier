/********************************************************************************
** Form generated from reading UI file 'time_table_manager.ui'
**
** Created by: Qt User Interface Compiler version 5.11.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TIME_TABLE_MANAGER_H
#define UI_TIME_TABLE_MANAGER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_time_table_manager
{
public:
    QWidget *centralwidget;
    QGroupBox *gridGroupBox_2;
    QGridLayout *gridLayout_4;
    QGroupBox *groupBox_4;
    QGroupBox *gridGroupBox;
    QGridLayout *gridLayout;
    QGroupBox *groupBox;
    QLineEdit *u_n_3;
    QLabel *label_7;
    QLineEdit *c_n_3;
    QLineEdit *E_box_3;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QLabel *label_profile;
    QLabel *label_setting;
    QLabel *label_P_notes;
    QGroupBox *gridGroupBox1;
    QGridLayout *gridLayout_6;
    QGroupBox *groupBox_6;
    QSpinBox *Row;
    QSpinBox *Column;
    QSpinBox *sem;
    QLineEdit *sec;
    QLineEdit *time;
    QLineEdit *Day;
    QLineEdit *C_no;
    QLineEdit *Course;
    QLineEdit *Teacher;
    QLabel *label_12;
    QLabel *label_13;
    QLabel *label_14;
    QLabel *label_15;
    QLabel *label_16;
    QLabel *label_17;
    QLabel *label_18;
    QLabel *label_19;
    QLabel *label_20;
    QPushButton *T_create;
    QLabel *T_info;
    QGroupBox *gridGroupBox4;
    QGridLayout *gridLayout_5;
    QGroupBox *groupBox_5;
    QPushButton *P_notes;
    QPushButton *Profile;
    QPushButton *C_table;
    QPushButton *Setting;
    QPushButton *T_table;
    QGroupBox *gridGroupBox2;
    QGridLayout *gridLayout_2;
    QGroupBox *groupBox_2;
    QLineEdit *u_n_2;
    QLabel *label_4;
    QLineEdit *c_n_2;
    QLineEdit *E_box_2;
    QLabel *C_pass1;
    QLineEdit *P_box;
    QLineEdit *C_pass;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_11;
    QPushButton *E_u_n;
    QLabel *Theme;
    QPushButton *E_c_n;
    QPushButton *E_email;
    QPushButton *E_pass;
    QGroupBox *gridGroupBox3;
    QGridLayout *gridLayout_3;
    QGroupBox *groupBox_3;
    QPushButton *add;
    QPlainTextEdit *plainTextEdit;

    void setupUi(QMainWindow *time_table_manager)
    {
        if (time_table_manager->objectName().isEmpty())
            time_table_manager->setObjectName(QStringLiteral("time_table_manager"));
        time_table_manager->resize(1200, 700);
        centralwidget = new QWidget(time_table_manager);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        gridGroupBox_2 = new QGroupBox(centralwidget);
        gridGroupBox_2->setObjectName(QStringLiteral("gridGroupBox_2"));
        gridGroupBox_2->setGeometry(QRect(179, 0, 1021, 701));
        gridLayout_4 = new QGridLayout(gridGroupBox_2);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        groupBox_4 = new QGroupBox(gridGroupBox_2);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        gridGroupBox = new QGroupBox(groupBox_4);
        gridGroupBox->setObjectName(QStringLiteral("gridGroupBox"));
        gridGroupBox->setGeometry(QRect(260, 172, 461, 341));
        gridLayout = new QGridLayout(gridGroupBox);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        groupBox = new QGroupBox(gridGroupBox);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        u_n_3 = new QLineEdit(groupBox);
        u_n_3->setObjectName(QStringLiteral("u_n_3"));
        u_n_3->setGeometry(QRect(130, 100, 191, 26));
        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(206, 70, 41, 31));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_7->sizePolicy().hasHeightForWidth());
        label_7->setSizePolicy(sizePolicy);
        QFont font;
        font.setPointSize(9);
        font.setBold(true);
        font.setUnderline(true);
        font.setWeight(75);
        label_7->setFont(font);
        c_n_3 = new QLineEdit(groupBox);
        c_n_3->setObjectName(QStringLiteral("c_n_3"));
        c_n_3->setGeometry(QRect(130, 163, 191, 26));
        E_box_3 = new QLineEdit(groupBox);
        E_box_3->setObjectName(QStringLiteral("E_box_3"));
        E_box_3->setGeometry(QRect(130, 226, 191, 26));
        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(187, 133, 91, 31));
        label_8->setFont(font);
        label_9 = new QLabel(groupBox);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(207, 196, 41, 31));
        label_9->setFont(font);
        label_10 = new QLabel(groupBox);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(109, 20, 251, 51));
        QFont font1;
        font1.setFamily(QStringLiteral("Palatino Linotype"));
        font1.setPointSize(19);
        font1.setBold(true);
        font1.setUnderline(true);
        font1.setWeight(75);
        label_10->setFont(font1);

        gridLayout->addWidget(groupBox, 0, 1, 1, 1);

        label_profile = new QLabel(groupBox_4);
        label_profile->setObjectName(QStringLiteral("label_profile"));
        label_profile->setGeometry(QRect(428, 106, 121, 41));
        QFont font2;
        font2.setFamily(QStringLiteral("Palatino Linotype"));
        font2.setPointSize(27);
        font2.setBold(true);
        font2.setUnderline(true);
        font2.setWeight(75);
        label_profile->setFont(font2);
        label_setting = new QLabel(groupBox_4);
        label_setting->setObjectName(QStringLiteral("label_setting"));
        label_setting->setGeometry(QRect(420, 105, 181, 51));
        label_setting->setFont(font2);
        label_P_notes = new QLabel(groupBox_4);
        label_P_notes->setObjectName(QStringLiteral("label_P_notes"));
        label_P_notes->setGeometry(QRect(360, 79, 261, 51));
        label_P_notes->setFont(font2);
        gridGroupBox1 = new QGroupBox(groupBox_4);
        gridGroupBox1->setObjectName(QStringLiteral("gridGroupBox1"));
        gridGroupBox1->setGeometry(QRect(225, 118, 541, 451));
        gridLayout_6 = new QGridLayout(gridGroupBox1);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        groupBox_6 = new QGroupBox(gridGroupBox1);
        groupBox_6->setObjectName(QStringLiteral("groupBox_6"));
        Row = new QSpinBox(groupBox_6);
        Row->setObjectName(QStringLiteral("Row"));
        Row->setGeometry(QRect(240, 28, 131, 26));
        Row->setCursor(QCursor(Qt::PointingHandCursor));
        Row->setMinimum(1);
        Row->setMaximum(10);
        Column = new QSpinBox(groupBox_6);
        Column->setObjectName(QStringLiteral("Column"));
        Column->setGeometry(QRect(240, 67, 131, 26));
        Column->setCursor(QCursor(Qt::PointingHandCursor));
        Column->setMinimum(1);
        Column->setMaximum(10);
        sem = new QSpinBox(groupBox_6);
        sem->setObjectName(QStringLiteral("sem"));
        sem->setGeometry(QRect(240, 107, 131, 26));
        sem->setCursor(QCursor(Qt::PointingHandCursor));
        sem->setMinimum(1);
        sem->setMaximum(8);
        sec = new QLineEdit(groupBox_6);
        sec->setObjectName(QStringLiteral("sec"));
        sec->setGeometry(QRect(240, 146, 131, 26));
        time = new QLineEdit(groupBox_6);
        time->setObjectName(QStringLiteral("time"));
        time->setGeometry(QRect(240, 186, 131, 26));
        Day = new QLineEdit(groupBox_6);
        Day->setObjectName(QStringLiteral("Day"));
        Day->setGeometry(QRect(240, 227, 131, 26));
        C_no = new QLineEdit(groupBox_6);
        C_no->setObjectName(QStringLiteral("C_no"));
        C_no->setGeometry(QRect(240, 266, 131, 26));
        Course = new QLineEdit(groupBox_6);
        Course->setObjectName(QStringLiteral("Course"));
        Course->setGeometry(QRect(240, 308, 131, 26));
        Teacher = new QLineEdit(groupBox_6);
        Teacher->setObjectName(QStringLiteral("Teacher"));
        Teacher->setGeometry(QRect(240, 348, 131, 26));
        label_12 = new QLabel(groupBox_6);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(160, 30, 41, 16));
        label_12->setFont(font);
        label_13 = new QLabel(groupBox_6);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(160, 70, 61, 16));
        label_13->setFont(font);
        label_14 = new QLabel(groupBox_6);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setGeometry(QRect(160, 110, 71, 16));
        label_14->setFont(font);
        label_15 = new QLabel(groupBox_6);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setGeometry(QRect(160, 150, 61, 16));
        label_15->setFont(font);
        label_16 = new QLabel(groupBox_6);
        label_16->setObjectName(QStringLiteral("label_16"));
        label_16->setGeometry(QRect(160, 190, 61, 16));
        label_16->setFont(font);
        label_17 = new QLabel(groupBox_6);
        label_17->setObjectName(QStringLiteral("label_17"));
        label_17->setGeometry(QRect(160, 230, 41, 16));
        label_17->setFont(font);
        label_18 = new QLabel(groupBox_6);
        label_18->setObjectName(QStringLiteral("label_18"));
        label_18->setGeometry(QRect(160, 270, 81, 16));
        label_18->setFont(font);
        label_19 = new QLabel(groupBox_6);
        label_19->setObjectName(QStringLiteral("label_19"));
        label_19->setGeometry(QRect(160, 310, 61, 21));
        label_19->setFont(font);
        label_20 = new QLabel(groupBox_6);
        label_20->setObjectName(QStringLiteral("label_20"));
        label_20->setGeometry(QRect(160, 350, 61, 16));
        label_20->setFont(font);
        T_create = new QPushButton(groupBox_6);
        T_create->setObjectName(QStringLiteral("T_create"));
        T_create->setGeometry(QRect(430, 389, 75, 30));
        QFont font3;
        font3.setPointSize(11);
        font3.setBold(true);
        font3.setUnderline(false);
        font3.setWeight(75);
        T_create->setFont(font3);
        T_create->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_6->addWidget(groupBox_6, 0, 0, 1, 1);

        T_info = new QLabel(groupBox_4);
        T_info->setObjectName(QStringLiteral("T_info"));
        T_info->setGeometry(QRect(400, 59, 181, 51));
        QFont font4;
        font4.setFamily(QStringLiteral("Palatino Linotype"));
        font4.setPointSize(27);
        font4.setBold(false);
        font4.setUnderline(true);
        font4.setWeight(50);
        T_info->setFont(font4);

        gridLayout_4->addWidget(groupBox_4, 0, 0, 1, 1);

        gridGroupBox4 = new QGroupBox(centralwidget);
        gridGroupBox4->setObjectName(QStringLiteral("gridGroupBox4"));
        gridGroupBox4->setGeometry(QRect(0, 0, 181, 701));
        gridLayout_5 = new QGridLayout(gridGroupBox4);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        groupBox_5 = new QGroupBox(gridGroupBox4);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        P_notes = new QPushButton(groupBox_5);
        P_notes->setObjectName(QStringLiteral("P_notes"));
        P_notes->setGeometry(QRect(8, 210, 141, 51));
        QFont font5;
        font5.setPointSize(12);
        font5.setBold(true);
        font5.setWeight(75);
        P_notes->setFont(font5);
        P_notes->setCursor(QCursor(Qt::PointingHandCursor));
        Profile = new QPushButton(groupBox_5);
        Profile->setObjectName(QStringLiteral("Profile"));
        Profile->setGeometry(QRect(8, 60, 141, 51));
        Profile->setFont(font5);
        Profile->setCursor(QCursor(Qt::PointingHandCursor));
        C_table = new QPushButton(groupBox_5);
        C_table->setObjectName(QStringLiteral("C_table"));
        C_table->setGeometry(QRect(8, 10, 141, 51));
        C_table->setFont(font5);
        C_table->setCursor(QCursor(Qt::PointingHandCursor));
        Setting = new QPushButton(groupBox_5);
        Setting->setObjectName(QStringLiteral("Setting"));
        Setting->setGeometry(QRect(8, 160, 141, 51));
        Setting->setFont(font5);
        Setting->setCursor(QCursor(Qt::PointingHandCursor));
        T_table = new QPushButton(groupBox_5);
        T_table->setObjectName(QStringLiteral("T_table"));
        T_table->setGeometry(QRect(8, 110, 141, 51));
        T_table->setFont(font5);
        T_table->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_5->addWidget(groupBox_5, 0, 0, 1, 1);

        gridGroupBox2 = new QGroupBox(centralwidget);
        gridGroupBox2->setObjectName(QStringLiteral("gridGroupBox2"));
        gridGroupBox2->setGeometry(QRect(416, 168, 511, 351));
        gridLayout_2 = new QGridLayout(gridGroupBox2);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        groupBox_2 = new QGroupBox(gridGroupBox2);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        u_n_2 = new QLineEdit(groupBox_2);
        u_n_2->setObjectName(QStringLiteral("u_n_2"));
        u_n_2->setGeometry(QRect(150, 50, 151, 26));
        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(60, 50, 81, 31));
        sizePolicy.setHeightForWidth(label_4->sizePolicy().hasHeightForWidth());
        label_4->setSizePolicy(sizePolicy);
        label_4->setFont(font);
        c_n_2 = new QLineEdit(groupBox_2);
        c_n_2->setObjectName(QStringLiteral("c_n_2"));
        c_n_2->setGeometry(QRect(150, 90, 151, 26));
        E_box_2 = new QLineEdit(groupBox_2);
        E_box_2->setObjectName(QStringLiteral("E_box_2"));
        E_box_2->setGeometry(QRect(150, 130, 151, 26));
        C_pass1 = new QLabel(groupBox_2);
        C_pass1->setObjectName(QStringLiteral("C_pass1"));
        C_pass1->setGeometry(QRect(60, 210, 131, 31));
        C_pass1->setFont(font);
        P_box = new QLineEdit(groupBox_2);
        P_box->setObjectName(QStringLiteral("P_box"));
        P_box->setGeometry(QRect(150, 170, 151, 26));
        P_box->setEchoMode(QLineEdit::Password);
        C_pass = new QLineEdit(groupBox_2);
        C_pass->setObjectName(QStringLiteral("C_pass"));
        C_pass->setGeometry(QRect(190, 210, 151, 26));
        C_pass->setEchoMode(QLineEdit::Password);
        label_5 = new QLabel(groupBox_2);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(60, 90, 91, 31));
        label_5->setFont(font);
        label_6 = new QLabel(groupBox_2);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(60, 130, 81, 31));
        label_6->setFont(font);
        label_11 = new QLabel(groupBox_2);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(60, 170, 81, 21));
        label_11->setFont(font);
        E_u_n = new QPushButton(groupBox_2);
        E_u_n->setObjectName(QStringLiteral("E_u_n"));
        E_u_n->setGeometry(QRect(310, 50, 41, 21));
        QPalette palette;
        QBrush brush(QColor(0, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(1, 14, 200, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        QBrush brush2(QColor(120, 120, 120, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        E_u_n->setPalette(palette);
        QFont font6;
        font6.setUnderline(true);
        E_u_n->setFont(font6);
        E_u_n->setCursor(QCursor(Qt::PointingHandCursor));
        E_u_n->setFlat(true);
        Theme = new QLabel(groupBox_2);
        Theme->setObjectName(QStringLiteral("Theme"));
        Theme->setGeometry(QRect(60, 215, 51, 21));
        Theme->setFont(font);
        E_c_n = new QPushButton(groupBox_2);
        E_c_n->setObjectName(QStringLiteral("E_c_n"));
        E_c_n->setGeometry(QRect(310, 90, 41, 21));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        E_c_n->setPalette(palette1);
        E_c_n->setFont(font6);
        E_c_n->setCursor(QCursor(Qt::PointingHandCursor));
        E_c_n->setFlat(true);
        E_email = new QPushButton(groupBox_2);
        E_email->setObjectName(QStringLiteral("E_email"));
        E_email->setGeometry(QRect(310, 130, 41, 21));
        QPalette palette2;
        palette2.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette2.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette2.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        palette2.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette2.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        E_email->setPalette(palette2);
        E_email->setFont(font6);
        E_email->setCursor(QCursor(Qt::PointingHandCursor));
        E_email->setFlat(true);
        E_pass = new QPushButton(groupBox_2);
        E_pass->setObjectName(QStringLiteral("E_pass"));
        E_pass->setGeometry(QRect(310, 170, 41, 21));
        QPalette palette3;
        palette3.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette3.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette3.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette3.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        E_pass->setPalette(palette3);
        E_pass->setFont(font6);
        E_pass->setCursor(QCursor(Qt::PointingHandCursor));
        E_pass->setFlat(true);

        gridLayout_2->addWidget(groupBox_2, 0, 0, 1, 1);

        gridGroupBox3 = new QGroupBox(centralwidget);
        gridGroupBox3->setObjectName(QStringLiteral("gridGroupBox3"));
        gridGroupBox3->setGeometry(QRect(296, 135, 771, 421));
        gridLayout_3 = new QGridLayout(gridGroupBox3);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        groupBox_3 = new QGroupBox(gridGroupBox3);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        add = new QPushButton(groupBox_3);
        add->setObjectName(QStringLiteral("add"));
        add->setGeometry(QRect(340, 13, 75, 23));
        QPalette palette4;
        palette4.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette4.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        palette4.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        add->setPalette(palette4);
        QFont font7;
        font7.setPointSize(10);
        font7.setBold(false);
        font7.setUnderline(true);
        font7.setWeight(50);
        add->setFont(font7);
        add->setCursor(QCursor(Qt::PointingHandCursor));
        add->setFlat(true);
        plainTextEdit = new QPlainTextEdit(groupBox_3);
        plainTextEdit->setObjectName(QStringLiteral("plainTextEdit"));
        plainTextEdit->setGeometry(QRect(10, 10, 731, 31));
        plainTextEdit->viewport()->setProperty("cursor", QVariant(QCursor(Qt::IBeamCursor)));
        plainTextEdit->raise();
        add->raise();

        gridLayout_3->addWidget(groupBox_3, 0, 0, 1, 1);

        time_table_manager->setCentralWidget(centralwidget);

        retranslateUi(time_table_manager);

        QMetaObject::connectSlotsByName(time_table_manager);
    } // setupUi

    void retranslateUi(QMainWindow *time_table_manager)
    {
        time_table_manager->setWindowTitle(QApplication::translate("time_table_manager", "MainWindow", nullptr));
        groupBox_4->setTitle(QString());
        label_7->setText(QApplication::translate("time_table_manager", "Name:", nullptr));
        label_8->setText(QApplication::translate("time_table_manager", "Contact No.:", nullptr));
        label_9->setText(QApplication::translate("time_table_manager", "Email:", nullptr));
        label_10->setText(QApplication::translate("time_table_manager", "Time Table Manager", nullptr));
        label_profile->setText(QApplication::translate("time_table_manager", "Profile:", nullptr));
        label_setting->setText(QApplication::translate("time_table_manager", "Settings:", nullptr));
        label_P_notes->setText(QApplication::translate("time_table_manager", "Personal Notes:", nullptr));
        groupBox_6->setTitle(QString());
        label_12->setText(QApplication::translate("time_table_manager", "Row:", nullptr));
        label_13->setText(QApplication::translate("time_table_manager", "Coloumn:", nullptr));
        label_14->setText(QApplication::translate("time_table_manager", "Semester:", nullptr));
        label_15->setText(QApplication::translate("time_table_manager", "Section:", nullptr));
        label_16->setText(QApplication::translate("time_table_manager", "Timings:", nullptr));
        label_17->setText(QApplication::translate("time_table_manager", "Days:", nullptr));
        label_18->setText(QApplication::translate("time_table_manager", "Course N0.:", nullptr));
        label_19->setText(QApplication::translate("time_table_manager", "Courses:", nullptr));
        label_20->setText(QApplication::translate("time_table_manager", "Teacher:", nullptr));
        T_create->setText(QApplication::translate("time_table_manager", "Create", nullptr));
        T_info->setText(QApplication::translate("time_table_manager", "Table Info:", nullptr));
        groupBox_5->setTitle(QString());
        P_notes->setText(QApplication::translate("time_table_manager", "Personal Notes", nullptr));
        Profile->setText(QApplication::translate("time_table_manager", "Profile", nullptr));
        C_table->setText(QApplication::translate("time_table_manager", "+Table", nullptr));
        Setting->setText(QApplication::translate("time_table_manager", "Settings", nullptr));
        T_table->setText(QApplication::translate("time_table_manager", "TimeTable", nullptr));
        groupBox_2->setTitle(QString());
        label_4->setText(QApplication::translate("time_table_manager", "User Name:", nullptr));
        C_pass1->setText(QApplication::translate("time_table_manager", "Confirm Password:", nullptr));
        label_5->setText(QApplication::translate("time_table_manager", "Contact No.:", nullptr));
        label_6->setText(QApplication::translate("time_table_manager", "Email:", nullptr));
        label_11->setText(QApplication::translate("time_table_manager", "Password:", nullptr));
        E_u_n->setText(QApplication::translate("time_table_manager", "Edit", nullptr));
        Theme->setText(QApplication::translate("time_table_manager", "Theme:", nullptr));
        E_c_n->setText(QApplication::translate("time_table_manager", "Edit", nullptr));
        E_email->setText(QApplication::translate("time_table_manager", "Edit", nullptr));
        E_pass->setText(QApplication::translate("time_table_manager", "Edit", nullptr));
        groupBox_3->setTitle(QString());
        add->setText(QApplication::translate("time_table_manager", "Add", nullptr));
    } // retranslateUi

};

namespace Ui {
    class time_table_manager: public Ui_time_table_manager {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TIME_TABLE_MANAGER_H
