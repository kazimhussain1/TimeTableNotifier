/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QGroupBox *groupBox_5;
    QPushButton *P_notes;
    QPushButton *Profile;
    QPushButton *Setting;
    QPushButton *T_table;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1304, 811);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        groupBox_5 = new QGroupBox(centralwidget);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        groupBox_5->setGeometry(QRect(10, 10, 157, 660));
        P_notes = new QPushButton(groupBox_5);
        P_notes->setObjectName(QStringLiteral("P_notes"));
        P_notes->setGeometry(QRect(8, 170, 141, 51));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        P_notes->setFont(font);
        P_notes->setCursor(QCursor(Qt::PointingHandCursor));
        Profile = new QPushButton(groupBox_5);
        Profile->setObjectName(QStringLiteral("Profile"));
        Profile->setGeometry(QRect(8, 20, 141, 51));
        Profile->setFont(font);
        Profile->setCursor(QCursor(Qt::PointingHandCursor));
        Setting = new QPushButton(groupBox_5);
        Setting->setObjectName(QStringLiteral("Setting"));
        Setting->setGeometry(QRect(8, 120, 141, 51));
        Setting->setFont(font);
        Setting->setCursor(QCursor(Qt::PointingHandCursor));
        T_table = new QPushButton(groupBox_5);
        T_table->setObjectName(QStringLiteral("T_table"));
        T_table->setGeometry(QRect(8, 70, 141, 51));
        T_table->setFont(font);
        T_table->setCursor(QCursor(Qt::PointingHandCursor));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 1304, 20));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        groupBox_5->setTitle(QString());
        P_notes->setText(QApplication::translate("MainWindow", "Personal Notes", nullptr));
        Profile->setText(QApplication::translate("MainWindow", "Profile", nullptr));
        Setting->setText(QApplication::translate("MainWindow", "Settings", nullptr));
        T_table->setText(QApplication::translate("MainWindow", "TimeTable", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
