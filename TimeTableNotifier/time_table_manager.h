#ifndef TIME_TABLE_MANAGER_H
#define TIME_TABLE_MANAGER_H

#include <QMainWindow>
#include <QString>

#include "newtable.h"


namespace Ui {
class time_table_manager;
}

class time_table_manager : public QMainWindow
{
    Q_OBJECT

public:
    explicit time_table_manager(QWidget *parent = 0);
    ~time_table_manager();

private slots:

    void on_Profile_clicked();

    void on_Setting_clicked();

    void on_P_notes_clicked();

    void on_add_clicked();

    void on_E_pass_clicked();

    void on_C_table_clicked();

    void on_T_create_clicked();

private:
    Ui::time_table_manager *ui;
    NewTable *NewTableForm;
};

#endif // TIME_TABLE_MANAGER_H
