#ifndef STUDENT_CLIENT_H
#define STUDENT_CLIENT_H

#include <QMainWindow>
#include <QTableWidget>
#include <QString>
#include <string>
//#include <fstream>
#include "TableClass/table.h"

namespace Ui {
class StudentClient;
}

class StudentClient : public QMainWindow
{
    Q_OBJECT

public:
    explicit StudentClient(QWidget *parent = 0);
    ~StudentClient();

private slots:
    void on_P_edit_clicked();

    void on_Profile_clicked();

    void on_Setting_clicked();

    void on_P_notes_clicked();

    void on_add_2_clicked();

    void on_E_pass_clicked();

    void on_T_table_clicked();

private:
    Ui::StudentClient *ui;
    Table *myTable;
    QTableWidget *DisplayTable;
};

#endif // STUDENT_CLIENT_H
