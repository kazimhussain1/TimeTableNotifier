#include "loadscreen.h"
#include "ui_loadscreen.h"

LoadScreen::LoadScreen(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LoadScreen)
{
    ui->setupUi(this);

    QPixmap bkgnd("C:/Users/Mehwish/Desktop/TimeTableNotifier/background.jpg");
    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Background, bkgnd);
    this->setPalette(palette);
    myTimer = new QTimer(this);
    connect(myTimer, SIGNAL(timeout()),this,SLOT(incrementProgressbar()));
    myTimer->start(10);//test krne k liey time kam kr dia hai main ne
    this->setFixedSize(QSize(1000, 650));
}

LoadScreen::~LoadScreen()
{
    delete ui;
}

void LoadScreen::incrementProgressbar()
{
    int currentValue =  ui->progressBar->value();
    if(currentValue >= 100)
    {
        myTimer->stop();
        this->hide();
        myLoginForm = new Login(this);
        myLoginForm->show();
    }
    ui->progressBar->setValue(++currentValue);

}
