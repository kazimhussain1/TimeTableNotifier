#include "sign_up.h"
#include "ui_sign_up.h"

sign_up::sign_up(QDialog *LoginForm, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::sign_up)
{
    ui->setupUi(this);
    myLoginForm = LoginForm;

    ui->label_A_Info->hide();
    ui->label_A_que->hide();
    ui->label_C_email->hide();
    ui->label_c_n->hide();
    ui->label_C_pass->hide();
    ui->label_E_box->hide();
    ui->label_P_box->hide();
    ui->label_Seatno->hide();
    ui->label_Sec->hide();
    ui->label_Sem->hide();
    ui->label_u_n->hide();
    ui->label_S_que->hide();
    ui->label_T_C->hide();
    ui->label_E_match->hide();
    ui->label_P_match->hide();


    ui->StudentParams->setStyleSheet("border:0;");
    ui->StudentParams->hide();

    ui->A_Info->setEditable(true);
    ui->A_Info->lineEdit()->setReadOnly(true);
    ui->A_Info->lineEdit()->setAlignment(Qt::AlignCenter);
    for (int i = 0 ; i < ui->A_Info->count() ; ++i)
    {
        ui->A_Info->setItemData(i, Qt::AlignCenter, Qt::TextAlignmentRole);
    }


    ui->S_que->setEditable(true);
    ui->S_que->lineEdit()->setReadOnly(true);
    ui->S_que->lineEdit()->setAlignment(Qt::AlignCenter);
    for (int i = 0 ; i < ui->S_que->count() ; ++i)
    {
        ui->S_que->setItemData(i, Qt::AlignCenter, Qt::TextAlignmentRole);
    }
    ui->E_box->setPlaceholderText("e.g:abc@gmail.com");
    ui->Seatno->setPlaceholderText("e.g:B12345678");
    this->setFixedSize(QSize(1000, 700));
}

sign_up::~sign_up()
{
    delete ui;
}


void sign_up::on_A_Info_currentIndexChanged(int index)
{
    if(ui->A_Info->currentText() == "Student")
    {
        ui->StudentParams->show();
    }
    else if(ui->A_Info->currentText() == "Professor")
    {
        ui->StudentParams->hide();
    }
    else if(ui->A_Info->currentText() == "Class Representative")
    {
        ui->StudentParams->show();
    }
    else if(ui->A_Info->currentText() == "Course Supervisor")
    {
        ui->StudentParams->hide();
    }
}

void sign_up::on_S_up_clicked()
{
    string username = ui->u_n->text().toStdString();
    int contact = (ui->c_n->text()).toInt();
    string email = ui->E_box->text().toStdString();
    string password = ui->P_box->text().toStdString();
    string sQuestion = ui->S_que->currentText().toStdString();
    string sAnswer = ui->S_ans->text().toStdString();
    int semester = (ui->Sem->text()).toInt();
    string section = ui->Sec->text().toStdString();
    string seatNo = ui->Seatno->text().toStdString();


    bool flag[12];
    bool match[2];


    // for all type of users
    if(username == "")
    {
        ui->label_u_n->show();
        flag[0] = true;
    }
    else
    {
        ui->label_u_n->hide();
        flag[0] = false;
    }
    if(contact == 0)
    {
        ui->label_c_n->show();
        flag[1] = true;
    }
    else
    {
        ui->label_c_n->hide();
        flag[1] = false;
    }
    if(email == "")
    {
        ui->label_E_box->show();
        flag[2] = true;
    }
    else
    {
        ui->label_E_box->hide();
        flag[2] = false;
    }
    if(password == "")
    {
        ui->label_P_box->show();
        flag[3] = true;
    }
    else
    {
        ui->label_P_box->hide();
        flag[3] = false;
    }
    if(sQuestion == "<<Select Here>>")
    {
        ui->label_S_que->show();
        flag[4] = true;
    }
    else
    {
        ui->label_S_que->hide();
        flag[4] = false;
    }
    if(sAnswer == "")
    {
        ui->label_A_que->show();
        flag[5] = true;
    }
    else
    {
        ui->label_A_que->hide();
        flag[5] = false;
    }
    if(ui->C_email->text() == "")
    {
        ui->label_C_email->show();
        flag[6] = true;
    }
    else
    {
        ui->label_C_email->hide();
        flag[6] = false;
        if(ui->C_email->text().toStdString() != email)
        {
            ui->label_E_match->show();
            match[0] = true;
        }
        else
        {
            ui->label_E_match->hide();
            match[0] = false;
        }
    }
    if(ui->C_pass->text() == "")
    {
        ui->label_C_pass->show();
        flag[7] = true;
    }
    else
    {
        ui->label_C_pass->hide();
        flag[7] = false;
        if(ui->C_pass->text().toStdString() != password)
        {
            ui->label_P_match->show();
            match[1] = true;
        }
        else
        {
            ui->label_P_match->hide();
            match[1] = false;
        }
    }
    if(ui->A_Info->currentText() == "<<Select Here>>")
    {
        ui->label_A_Info->show();
        flag[8] = true;
    }
    else
    {
        ui->label_A_Info->hide();
        flag[8] = false;
    }


    //for student and CR
    if(semester == 0)
    {
        ui->label_Sem->show();
        flag[9] = true;
    }
    else
    {
        ui->label_Sem->hide();
        flag[9] = false;
    }
    if(section == "")
    {
        ui->label_Sec->show();
        flag[10] = true;
    }
    else
    {
        ui->label_Sec->hide();
        flag[10] = false;
    }
    if(seatNo == "")
    {
        ui->label_Seatno->show();
        flag[11] = true;
    }
    else
    {
        ui->label_Seatno->hide();
        flag[11] = false;
    }


    int check = 0;
    if(ui->A_Info->currentText() == "Student" || ui->A_Info->currentText() == "Class Representative")
    {
        for(int i = 0; i<12;i++)
        {
            if(flag[i] == true)
                check++;
        }
    }
    else if(ui->A_Info->currentText() == "Professor" || ui->A_Info->currentText() == "Course Supervisor")
    {
        for(int i = 0; i<9;i++)
        {
            if(flag[i] == true)
                check++;
        }
    }
    else
    {
        check++;
    }
    if(check == 0 && match[0] == false && match[1] == false)
    {
        this->hide();
        myStudent = new Student(username, contact, email, password, sQuestion, sAnswer, semester, section, seatNo);

        ofstream myFile;
        myFile.open("Users.data", std::fstream::app);
        myFile << ui->A_Info->currentText().toStdString();
        myFile << *myStudent;
        myFile.close();
        myLoginForm->show();
    }
}
