#ifndef FORGET_PASS_H
#define FORGET_PASS_H

#include <QMainWindow>

namespace Ui {
class forget_pass;
}

class forget_pass : public QMainWindow
{
    Q_OBJECT

public:
    explicit forget_pass(QWidget *parent = 0);
    ~forget_pass();

private slots:
    void on_F_acc_clicked();

    void on_S_code_clicked();

    void on_S_question_clicked();

    void on_N_password_clicked();

private:
    Ui::forget_pass *ui;
};

#endif // FORGET_PASS_H
