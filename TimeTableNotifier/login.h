#ifndef LOGIN_H
#define LOGIN_H

#include <QDialog>
#include <QMessageBox>
#include <vector>
#include <bits/stdc++.h>

#include "forget_pass.h"
#include "sign_up.h"
#include "time_table_manager.h"
#include "student_client.h"
#include "user_class/student.h"

namespace Ui {
class Login;
}

class Login : public QDialog
{
    Q_OBJECT

public:
    explicit Login(QWidget *parent = 0);
    ~Login();


private slots:
    void on_Log_in_clicked();

    void on_F_pass_clicked();

    void on_Signup_clicked();

private:
    Ui::Login *ui;
    time_table_manager *MyManagerProfile;
    sign_up *SignUpPage;
    forget_pass *SetPassPage;
    StudentClient *MyStudentProfile;
    User* myUser;
    vector <User*> UserArray;

};

#endif // LOGIN_H
