#include "newtable.h"
#include "ui_newtable.h"

NewTable::NewTable(int row, int column, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NewTable)
{
    ui->setupUi(this);
    this->setFixedSize(QSize(1200, 700));


    DisplayTable = new QTableWidget(row, column, this);
    DisplayTable->setGeometry(100,100,700,400);

    SessionArray = new vector<vector<SessionDictionary*>>(row);
    MyNewTable = new Table(row);

    connect(this->DisplayTable, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(DoubleClickEvent(int,int)));
}

NewTable::~NewTable()
{
    delete ui;
}

void NewTable::DoubleClickEvent(int row,int column)
{
    myInfo = new Info(SessionArray, DisplayTable, row, column,this);
    myInfo->setWindowModality(Qt::WindowModal);
    myInfo->show();
}

void NewTable::on_Submit_clicked()
{
    for(unsigned int i=0;i<SessionArray->size();i++)
    {
        for(unsigned int j=0;j<(*SessionArray)[i].size() - 1;j++)
        {
            for(unsigned int k=0;k<(*SessionArray)[i].size() -1 - j;k++)
            {
                if(((*SessionArray)[i][k])->key > ((*SessionArray)[i][k+1])->key)
                {
                    SessionDictionary *temp = (*SessionArray)[i][k];
                    (*SessionArray)[i][k] = (*SessionArray)[i][k+1];
                    (*SessionArray)[i][k+1] = temp;

                }
            }
        }
    }
    vector<vector<SessionDictionary*>>::iterator i;

    for(i = SessionArray->begin();i<SessionArray->end();i++)
    {
        vector<SessionDictionary*>::iterator j;

        Day *tempDay;
        tempDay = new Day(i->size());
        for(j = i->begin();j < i->end();j++)
        {
            tempDay->AddSession(*((*j)->getSession()));
        }
        this->MyNewTable->AddDay(*tempDay);
        delete tempDay;
    }
    ofstream outfile("table1.txt");
    outfile << *MyNewTable;
}
