#include "session.h"

Session::Session()
{
    this->Course = "";
    this->CourseNo = 0;
    this->TeacherName = "";
    this->TimePeriod = 0;
}
Session::Session(string course, int courseNo, string name, int TimePeriod)
{
    this->Course = course;
    this->CourseNo = courseNo;
    this->TeacherName = name;
    this->TimePeriod = TimePeriod;
}
Session::Session(Session& Copy)
{
    this->Course = Copy.Course;
    this->CourseNo = Copy.CourseNo;
    this->TeacherName = Copy.TeacherName;
    this->TimePeriod = Copy.TimePeriod;
}
Session::~Session()
{

}

string Session::getCourse()
{
    return this->Course;
}
int Session::getCourseNo()
{
    return this->CourseNo;
}
string Session::getTeacherName()
{
    return this->TeacherName;
}
int Session::getTimePeriod()
{
    return this->TimePeriod;
}


void Session::setCourse(string course)
{
    this->Course = course;
}
void Session::setCourseNo(int courseNo)
{
    this->CourseNo = courseNo;
}
void Session::setTeacherName(string name)
{
    this->TeacherName = name;
}
void Session::setTimePeriod(int TimePeriod)
{
    this->TimePeriod = TimePeriod;
}



ofstream& operator << (ofstream& OutFile,Session S)
{
    OutFile <<S.Course<<endl<<S.CourseNo<<endl<<S.TeacherName<<endl<<S.TimePeriod<<endl;
    return OutFile;
}

ifstream& operator >> (ifstream& InFile, Session& S)
{
    InFile >> S.Course;
    InFile >> S.CourseNo;
    InFile >> S.TeacherName;
    InFile >> S.TimePeriod;
    qInfo() << "session";
    return InFile;
}






