#include "table.h"

Table::Table()
{
    this->MaxDays = 5;
    this->CurrentDays = 0;
    this->WorkDays = new Day[5];
}
Table::Table(int MaxDays)
{
    this->MaxDays = MaxDays;
    this->CurrentDays = 0;
    this->WorkDays = new Day[MaxDays];
}
Table::Table(Table& Copy)
{
    this->MaxDays = Copy.MaxDays;
    this->CurrentDays = Copy.CurrentDays;
    /*if(this->WorkDays != nullptr)
        delete[] this->WorkDays;*/
    this->WorkDays = new Day[MaxDays];
    for(int i = 0; i< Copy.MaxDays; i++)
    {
        this->WorkDays[i] = Copy.WorkDays[i];
    }
}
Table::~Table()
{
    delete[] this->WorkDays;
}


int Table::getCurrentDays()
{
    return this->CurrentDays;
}
int Table::getMaxDays()
{
    return this->MaxDays;
}


void Table::AddDay(Day myDay)
{
    if(this->CurrentDays < this->MaxDays)
    {
        this->WorkDays[this->CurrentDays] = myDay;
        this->CurrentDays++;
    }
    else
    {
        IncreaseSize();
        AddDay(myDay);
    }
}

void Table::IncreaseSize()
{
    this->MaxDays++;
    Day *temp = new Day[this->MaxDays];
    memcpy(temp, this->WorkDays, sizeof(Day)*CurrentDays);
    delete[] this->WorkDays;
    WorkDays = temp;
}

Day& Table::operator [](int index)
{
    if(index <= this->MaxDays)
    {
        return this->WorkDays[index];
    }
}

void Table::display(QTableWidget *DisplayTable, QMainWindow *window)
{
    DisplayTable->setRowCount(5);
    DisplayTable->setColumnCount(7);
    DisplayTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
    DisplayTable->setGeometry(100,100,785,400);
    DisplayTable->setVerticalHeaderLabels({"Monday", "Tuesday", "Wednesday", "Thursday", "Friday"});
    DisplayTable->setHorizontalHeaderLabels({"9:00-9:50", "9:50-1:40", "11:00-11:50", "12:00-12:50", "12:50-1:50", "1:50-2:40", "2:40-3:30"});
    DisplayTable->setSpan(0,4,5,1);

    DisplayTable->setStyleSheet(
                "QHeaderView::section{"
                    "border-top:0px solid #D8D8D8;"
                    "border-left:0px solid #D8D8D8;"
                    "border-right:1px solid #D8D8D8;"
                    "border-bottom: 1px solid #D8D8D8;"
                    "background-color:white;"
                    "padding:4px;"
                "}"
                "QTableCornerButton::section{"
                    "border-top:0px solid #D8D8D8;"
                    "border-left:0px solid #D8D8D8;"
                    "border-right:1px solid #D8D8D8;"
                    "border-bottom: 1px solid #D8D8D8;"
                    "background-color:white;"
                "}");




    QTableWidgetItem *temp = new QTableWidgetItem("Lunch Break");
    DisplayTable->setItem(0, 4, temp);

    int i,j;

    for(i=0;i<this->getMaxDays();i++)
    {
        DisplayTable->setRowHeight(i,50);
    }

    int HourCount=0;
    Session myArray[this->getCurrentDays()][4];

    for(i=0;i<this->getMaxDays();i++)
    {
        HourCount = 0;
        for(j=0;j < (this->operator [](i)).getMaxSessions()  ;j++)
        {
            myArray[i][j] = (this->operator [](i)).operator [](j);
            if(myArray[i][j].getTimePeriod() == 2)
            {
                DisplayTable->setSpan(i,(HourCount == 4?5:HourCount) ,1,2);
                temp = new QTableWidgetItem(QString::fromStdString(myArray[i][j].getCourse()));
                DisplayTable->setItem(i,(HourCount == 4?5:HourCount),temp);
                HourCount+=2;
            }
            else if(myArray[i][j].getTimePeriod() == 1)
            {
                DisplayTable->setSpan(i,(HourCount == 4?5:HourCount) ,1,1);
                temp = new QTableWidgetItem(QString::fromStdString(myArray[i][j].getCourse()));
                DisplayTable->setItem(i,(HourCount == 4?5:HourCount),temp);
                HourCount++;
            }
            else if(myArray[i][j].getTimePeriod() == 3)
            {
                DisplayTable->setSpan(i,(HourCount == 4?5:HourCount) ,1,3);
                temp = new QTableWidgetItem(QString::fromStdString(myArray[i][j].getCourse()));
                DisplayTable->setItem(i,(HourCount == 4?5:HourCount),temp);
                HourCount+=3;
            }
            HourCount == 4?HourCount =5:HourCount = HourCount;
        }
    }
}



ofstream& operator <<(ofstream& OutFile, Table& T)
{
    OutFile << T.CurrentDays<< endl << T.MaxDays << endl;
    for(int i = 0; i < T.CurrentDays; i++)
    {
        OutFile << "Day " << (i+1) << endl;
        OutFile << T.WorkDays[i];
    }
    return OutFile;
}

ifstream& operator >>(ifstream& InFile, Table& T)
{
    qInfo()<<"Table1";
    InFile >> T.CurrentDays;
    InFile >> T.MaxDays;
    qInfo()<<"Table2";
    //T.WorkDays = new Day[T.MaxDays];
    for(int i = 0; i < T.MaxDays; i++)
    {
        qInfo()<<"Table3";
        InFile >> T.WorkDays[i];
    }
    qInfo()<<"Table4";
    return InFile;
}
