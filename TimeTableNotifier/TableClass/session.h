#ifndef SESSION_H
#define SESSION_H

#include <string>
#include <iostream>
#include <fstream>
#include <QDebug>

using namespace std;

class Session
{
public:
    Session();
    Session(string, int, string, int);
    Session(Session&);
    ~Session();

    string getCourse();
    int getCourseNo();
    string getTeacherName();
    int getTimePeriod();

    void setCourse(string);
    void setCourseNo(int);
    void setTeacherName(string);
    void setTimePeriod(int);

    friend ofstream& operator << (ofstream& OutFile, Session S);
    friend ifstream& operator >> (ifstream& InFile, Session& S);

private:
    string Course;
    int CourseNo;
    string TeacherName;
    int TimePeriod;
};




#endif // SESSION_H
