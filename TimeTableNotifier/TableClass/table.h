#ifndef TABLE_H
#define TABLE_H

#include <iostream>
#include <QString>
#include <string>
#include "Day.h"
#include <QTableWidget>
#include <QMainWindow>

using namespace std;

class Table
{
public:
    Table();
    Table(int);
    Table(Table&);
    ~Table();

    int getCurrentDays();
    int getMaxDays();

    void AddDay(Day);

    Day& operator[] (int);
    void display(QTableWidget*, QMainWindow*);


    friend ofstream& operator <<(ofstream& OutFile, Table& T);
    friend ifstream& operator >>(ifstream& InFile, Table& T);

private:
    Day * WorkDays;//rows
    int CurrentDays;
    int MaxDays;
    void IncreaseSize();
};

#endif // TABLE_H
