#ifndef DAY_H
#define DAY_H

#include<iostream>
#include<string>
#include"Session.h"

using namespace std;

class Day
{
public:
    Day();
    Day(int);
    Day(Day&);
    ~Day();

    int getCurrentSessions();
    int getMaxSessions();

    void AddSession(Session);

    Session& operator [](int);
    Day& operator = (Day&);

    friend ofstream& operator <<(ofstream& OutFile, Day D);
    friend ifstream& operator >>(ifstream& InFile, Day& D);

private:
    Session *Sessions;//columns
    int CurrentSessions;
    int MaxSessions;
    void IncreaseSize();
};

#endif // DAY_H
