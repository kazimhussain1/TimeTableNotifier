#include "day.h"

Day::Day()
{
    this->CurrentSessions = 0;
    this->MaxSessions = 3;
    this->Sessions = new Session[3];
}
Day::Day(int MaxSessions)
{
    this->CurrentSessions = 0;
    this->MaxSessions = MaxSessions;
    this->Sessions = new Session[MaxSessions];
}
Day::Day(Day& Copy)
{
    this->CurrentSessions = Copy.CurrentSessions;
    this->MaxSessions = Copy.MaxSessions;
    /*if(this->Sessions != nullptr)
        delete[] this->Sessions;*/
    this->Sessions = new Session[Copy.MaxSessions];
    for(int i=0;i < Copy.CurrentSessions; i++)
    {
        this->Sessions[i] = Copy.Sessions[i];
    }
}
Day::~Day()
{
    delete[] this->Sessions;
}

int Day::getCurrentSessions()
{
    return this->CurrentSessions;
}
int Day::getMaxSessions()
{
    return this->MaxSessions;
}

void Day::AddSession(Session mySession)
{
    if(this->CurrentSessions < this->MaxSessions)
    {
        this->Sessions[this->CurrentSessions] = mySession;
        this->CurrentSessions++;
    }
    else
    {
        IncreaseSize();
        AddSession(mySession);
    }
}

Session& Day::operator [](int index)
{
    if(index < this->MaxSessions)
    {
        return this->Sessions[index];
    }
}
Day& Day::operator = (Day& Copy)
{
    this->CurrentSessions = Copy.CurrentSessions;
    this->MaxSessions = Copy.MaxSessions;
    /*if(this->Sessions != nullptr)
        delete[] this->Sessions;*/

    this->Sessions = new Session[Copy.MaxSessions];
    for(int i=0;i < Copy.MaxSessions; i++)
    {
        this->Sessions[i] = Copy.Sessions[i];
    }
    return *this;
}

void Day::IncreaseSize()
{
    this->MaxSessions++;
    Session *temp = new Session[MaxSessions];
    memcpy(temp, this->Sessions, sizeof(Session)*this->CurrentSessions);
    delete[] this->Sessions;
    this->Sessions = temp;
    temp = NULL;
}


ofstream& operator <<(ofstream& OutFile, Day D)
{
     OutFile << D.CurrentSessions << endl << D.MaxSessions << endl;
     for(int i=0;i<D.CurrentSessions;i++)
     {
         OutFile << "Session "<< (i+1)<< endl;
         OutFile << D.Sessions[i];
     }
     return OutFile;
}
ifstream& operator >>(ifstream& InFile, Day& D)
{
    string skip;

    InFile >> skip >>  skip;
    InFile >> D.CurrentSessions;
    InFile >> D.MaxSessions;

    D.Sessions = new Session[D.MaxSessions];
    qInfo()<<"Day";
    for(int i = 0; i < D.CurrentSessions; i++)
    {
        InFile >> skip >> skip;
        InFile >> D.Sessions[i];
    }
    return InFile;
}
