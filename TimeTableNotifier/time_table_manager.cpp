#include "time_table_manager.h"
#include "ui_time_table_manager.h"

time_table_manager::time_table_manager(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::time_table_manager)
{
    ui->setupUi(this);
    ui->gridGroupBox->hide();
    ui->label_profile->hide();
    ui->gridGroupBox2->hide();
    ui->label_setting->hide();
    ui->plainTextEdit->hide();
    ui->gridGroupBox3->hide();
    ui->label_P_notes->hide();
    ui->C_pass->hide();
    ui->C_pass1->hide();
    ui->T_info->hide();
    ui->gridGroupBox1->hide();
    this->setFixedSize(QSize(1200, 700));
}

time_table_manager::~time_table_manager()
{
    delete ui;
}

void time_table_manager::on_Profile_clicked()
{
    ui->gridGroupBox->show();
    ui->label_profile->show();
    ui->T_info->hide();
    ui->gridGroupBox1->hide();
    ui->gridGroupBox2->hide();
    ui->label_setting->hide();
    ui->gridGroupBox3->hide();
    ui->label_P_notes->hide();
}

void time_table_manager::on_Setting_clicked()
{
    ui->gridGroupBox2->show();
    ui->label_setting->show();
    ui->gridGroupBox->hide();
    ui->label_profile->hide();
    ui->T_info->hide();
    ui->gridGroupBox1->hide();
    ui->gridGroupBox3->hide();
    ui->label_P_notes->hide();
}

void time_table_manager::on_P_notes_clicked()
{
    ui->gridGroupBox3->show();
    ui->label_P_notes->show();
    ui->gridGroupBox->hide();
    ui->label_profile->hide();
    ui->T_info->hide();
    ui->gridGroupBox1->hide();
    ui->gridGroupBox2->hide();
    ui->label_setting->hide();
}

void time_table_manager::on_add_clicked()
{
    ui->plainTextEdit->show();
    ui->add->setGeometry(340,41,75,23);
}

void time_table_manager::on_E_pass_clicked()
{
    ui->C_pass->show();
    ui->C_pass1->show();
    ui->Theme->setGeometry(60,251,51,21);
}

void time_table_manager::on_C_table_clicked()
{
    ui->T_info->show();
    ui->gridGroupBox1->show();
    ui->gridGroupBox->hide();
    ui->label_profile->hide();
    ui->gridGroupBox2->hide();
    ui->label_setting->hide();
    ui->gridGroupBox3->hide();
    ui->label_P_notes->hide();
}

void time_table_manager::on_T_create_clicked()
{
    this->hide();
    NewTableForm = new NewTable(ui->Row->text().toInt(), ui->Column->text().toInt());
    NewTableForm->show();

}
