#ifndef SIGN_UP_H
#define SIGN_UP_H

#include <QWidget>
#include <QDialog>
#include <QString>
#include <fstream>

#include "user_class/student.h"

namespace Ui {
class sign_up;
}

class sign_up : public QWidget
{
    Q_OBJECT

public:
    explicit sign_up(QDialog *LoginForm, QWidget *parent = 0);
    ~sign_up();

private slots:

    void on_S_up_clicked();

    void on_A_Info_currentIndexChanged(int index);

private:
    Ui::sign_up *ui;
    QDialog *myLoginForm;
    Student *myStudent;
};

#endif // SIGN_UP_H
