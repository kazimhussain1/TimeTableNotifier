#ifndef SESSIONDICTIONARY_H
#define SESSIONDICTIONARY_H

#include "TableClass/session.h"

class SessionDictionary
{
public:
    SessionDictionary(int, Session*);
    Session* getSession();

    int key;

private:
    Session *value;
};

#endif // SESSIONDICTIONARY_H
