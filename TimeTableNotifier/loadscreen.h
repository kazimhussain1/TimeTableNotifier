#ifndef LOADSCREEN_H
#define LOADSCREEN_H

#include <QWidget>
#include <QPixmap>
#include <QPalette>
#include <QTimer>
#include "login.h"

namespace Ui {
class LoadScreen;
}

class LoadScreen : public QWidget
{
    Q_OBJECT

public:
    explicit LoadScreen(QWidget *parent = 0);
    ~LoadScreen();

public slots:
    void incrementProgressbar();

private:
    Ui::LoadScreen *ui;
    QTimer *myTimer;
    Login *myLoginForm;
};

#endif // LOADSCREEN_H
