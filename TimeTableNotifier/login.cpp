#include "login.h"
#include "ui_login.h"

Login::Login(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Login)
{
    ui->setupUi(this);
    this->setFixedSize(QSize(1000, 650));
    ui->Info->hide();


    ifstream myFile("Users.data");
    if(myFile)
    {


        User *myUser;
        Student *myStudent;

        string skip;
        while(!myFile.eof())
        {
            myStudent = new Student();
            myFile >> *myStudent;
            myUser = myStudent;
            UserArray.push_back(myUser);
            myFile >> skip;
        }
        myFile.close();
    }
    else
    {
        QMessageBox MyError;
        MyError.setText("Missing Essential Component");
        MyError.exec();
        QApplication::quit();
    }


}

Login::~Login()
{
    delete ui;
}


void Login::on_Log_in_clicked()
{
    QString email,password;
    email = ui->Email->text();
    password = ui->Pass->text();

    vector<User*>::iterator i;
    for(i = UserArray.begin(); i < UserArray.end(); ++i)
    {
        if(email == "test" && password == "test")
        {
            this->hide();
            MyManagerProfile = new time_table_manager(this);
            MyManagerProfile->show();
        }
        else if(email.toStdString() == (*i)->getEmail() && password.toStdString() == (*i)->getPassword())
        {
            this->hide();
            MyStudentProfile = new StudentClient(this);
            MyStudentProfile->show();
        }
        else
        {
            ui->Info->show();
            ui->F_pass->show();
            ui->F_pass->setGeometry(100,200,111,23);
        }
    }
}

void Login::on_F_pass_clicked()
{
    this->hide();
    SetPassPage = new forget_pass(this);
    SetPassPage->show();
}

void Login::on_Signup_clicked()
{
    this->hide();
    SignUpPage = new sign_up(this);
    SignUpPage->show();
}
