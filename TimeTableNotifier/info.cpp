#include "info.h"
#include "ui_info.h"

Info::Info(vector<vector<SessionDictionary*>> *SessionArray, QTableWidget *DisplayTable, int row, int column, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Info)
{
    ui->setupUi(this);
    this->SessionArray = SessionArray;
    this->DisplayTable = DisplayTable;
    this->row = row;
    this->column = column;
}

Info::~Info()
{
    delete ui;
}

void Info::on_buttonBox_accepted()
{
    Session *temp = new Session(ui->Course->text().toStdString(), ui->CourseNo->text().toInt(), ui->Name->text().toStdString(), ui->TimePeriod->text().toInt());
    SessionDictionary *tempDict = new SessionDictionary(column, temp);
    (*SessionArray)[row].push_back(tempDict);
    DisplayTable->setSpan(row, column, 1, temp->getTimePeriod());
    QTableWidgetItem *MyItem = new QTableWidgetItem(ui->Course->text());
    MyItem->setTextAlignment(Qt::AlignCenter);
    DisplayTable->setItem(row, column, MyItem);
}
