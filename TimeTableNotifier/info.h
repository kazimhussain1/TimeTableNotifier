#ifndef INFO_H
#define INFO_H

#include <QDialog>
#include <QString>
#include <QTableWidget>
#include <vector>

#include "TableClass/table.h"
#include "Misc/sessiondictionary.h"
//#include "newtable.h"

namespace Ui {
class Info;
}

class Info : public QDialog
{
    Q_OBJECT

public:
    explicit Info(vector<vector<SessionDictionary*>> *SessionArray,QTableWidget *DisplayTable, int row, int column, QWidget *parent = 0);
    ~Info();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::Info *ui;
    vector<vector<SessionDictionary*> > *SessionArray;
    QTableWidget *DisplayTable;

    int row;
    int column;
};

#endif // INFO_H
