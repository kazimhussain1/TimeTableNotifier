#ifndef PROFESSOR_H
#define PROFESSOR_H
#include "user.h"

class Professor:public user
{
public:
Professor():user()
{
  this->no_of_subs=5;
  this->subjects= new session[this->no_of_subs];
}


Professor(session *sub,int no,string un, long contact, string email, string pass, string sq, string sa):user(un,contact,email,pass,sq,sa)
{
  this->subjects= new session[no];
  this->no_of_subs=no;
  for(int i=0;i<no;i++)
  {
    this->subjects[i]= sub[i];
  }
}


Professor(Professor& copyProf):user(copyProf)
{
   this->no_of_subs= copyProf.no_of_subs;
   this->subjects= new session[this->no_of_subs];
    for(int i=0;i<this->no_of_subs;i++)
  {
    this->subjects[i]= copyProf.subjects[i];
  }

}

virtual ~Professor()
{
  delete subjects;
  subjects= nullptr;
}

session getsubs(int sub_no)
{
  return this->subjects[sub_no];
}


virtual void display()
{
  cout<<*this;
}

friend ostream& operator << (ostream& out, Professor& prof)
{
   out<<(user)prof<<endl;

   for(int i=0;i<prof.no_of_subs;i++)
   {
     out<<prof.subjects[i]<<endl;
   }
   return out;
}


protected:
session *subjects;
int no_of_subs;

};

#endif // PROFESSOR_H
