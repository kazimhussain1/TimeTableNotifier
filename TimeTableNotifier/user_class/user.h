#ifndef USER_H
#define USER_H

#include <string>
#include <QString>

#include "TableClass/session.h"


class User
{
public:
    User();
    User(string UserName, long long int ContactNo, string Email, string Password, string SecretQuestion, string SecretAnswer);
    //User(User& Ucopy);
    virtual ~User();




    void setUserName(string UserName);
    void setContactNo(long long int ContactNo);
    void setEmail(string Email);
    void setPassword(string Password);
    void setSecretQuestion(string SecretQuestion);
    void setSecretAnswer(string SecretAnswer);

    string getUserName();
    long long int getContactNo();
    string getEmail();
    string getPassword();
    string getSecretQuestion();
    string getSecretAnswer();

    User& ConvertToUser();

    friend ofstream& operator << (ofstream& OutFile, User U);
    friend ifstream& operator >> (ifstream& InFile, User& U);



private:
    string UserName;
    long long int ContactNo;
    string Email;
    string Password;
    string SecretQuestion;
    string SecretAnswer;
};

#endif // USER_H
