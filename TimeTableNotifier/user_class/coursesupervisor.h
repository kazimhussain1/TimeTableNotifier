#ifndef COURSESUPERVISOR_H
#define COURSESUPERVISOR_H

#include "user_class/user.h"

class CourseSupervisor : public user
{
public:
    CourseSupervisor():Professor()
    {

    }


    CourseSupervisor(session *sub,int no,string un, long contact, string email, string pass, string sq, string sa):Professor(sub,no,un,contact,email,pass,sq,sa)
    {

    }


    CourseSupervisor(CourseSupervisor& cscopy):Professor(cscopy)
    {


    }

    ~CourseSupervisor()
    {

    }

    void display()
    {
      cout<<*this;
    }


    friend ostream& operator << (ostream& out, CourseSupervisor& cs)
    {
      return out <<(Professor)cs <<endl;
    }

    };


};

#endif // COURSESUPERVISOR_H
