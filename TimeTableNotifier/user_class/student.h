#ifndef STUDENT_H
#define STUDENT_H

#include "user.h"


class Student: public User
{
public:
    Student();
    Student(string username, long long int contact, string email, string pass, string sq, string sa, int Semester, string sec, string SeatNum);
    Student(Student& scopy);

    virtual ~Student();


    int getSemester();
    string getsection();
    Session getcourse(int no);




    friend ofstream& operator << (ofstream& OutFile, Student S);
    friend ifstream& operator >> (ifstream& InFile, Student& S);

protected:
    int Semester;
    string section;
    string SeatNum;
    };



#endif // STUDENT_H
