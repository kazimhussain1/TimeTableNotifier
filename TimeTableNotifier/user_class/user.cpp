#include "user.h"

User::User()
{
    this->UserName = "";
    this->ContactNo = 0;
    this->Email = "";
    this->Password = "";
    this->SecretQuestion = "";
    this->SecretAnswer = "";
}
User::User(string UserName, long long int ContactNo, string Email, string Password, string SecretQuestion, string SecretAnswer)
{
    this->UserName = UserName;
    this->ContactNo = ContactNo;
    this->Email = Email;
    this->Password = Password;
    this->SecretQuestion = SecretQuestion;
    this->SecretAnswer = SecretAnswer;
}
/*User::User(User& Ucopy)
{
    this->UserName = Ucopy.UserName;
    this->ContactNo = Ucopy.ContactNo;
    this->Email = Ucopy.Email;
    this->Password = Ucopy.Password;
    this->SecretQuestion = Ucopy.SecretQuestion;
    this->SecretAnswer = Ucopy.SecretAnswer;
}*/
User::~User()
{

}


void User::setUserName(string UserName)
{
    this->UserName = UserName;
}
void User::setContactNo(long long int ContactNo)
{
    this->ContactNo = ContactNo;
}
void User::setEmail(string Email)
{
    this->Email = Email;
}
void User::setPassword(string Password)
{
    this->Password = Password;
}
void User::setSecretQuestion(string SecretQuestion)
{
    this->SecretQuestion = SecretQuestion;
}
void User::setSecretAnswer(string SecretAnswer)
{
    this->SecretAnswer = SecretAnswer;
}

string User::getUserName()
{
    return this->UserName;
}
long long int User::getContactNo()
{
    return this->ContactNo;
}
string User::getEmail()
{
    return this->Email;
}
string User::getPassword()
{
    return this->Password;
}
string User::getSecretQuestion()
{
    return this->SecretQuestion;
}
string User::getSecretAnswer()
{
    return this->SecretAnswer;
}

User& User::ConvertToUser()
{
    return *this;
}


ofstream& operator << (ofstream& OutFile, User U)
{
    OutFile << U.UserName << endl;
    OutFile << U.ContactNo << endl;
    OutFile << U.Email << endl;
    OutFile << U.Password << endl;
    OutFile << U.SecretQuestion << endl;
    OutFile << U.SecretAnswer << endl;
    return OutFile;
}
ifstream& operator >> (ifstream& InFile, User& U)
{
    string skip;
    getline(InFile, U.UserName);
    InFile >> U.ContactNo;
    InFile >> U.Email;
    InFile >> U.Password;
    getline(InFile, skip);
    getline(InFile, U.SecretQuestion);
    //getline(InFile, skip);
    getline(InFile, U.SecretAnswer);
    return InFile;
}

