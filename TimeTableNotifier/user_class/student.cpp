#include "student.h"


Student::Student():User()
{
      this->Semester = 0;
      this->section= 'A';
}

Student::Student(string username, long long int contact, string email, string pass, string sq, string sa,int Semester,string sec, string SeatNum):User(username,contact,email,pass,sq,sa)
{
    this->Semester=Semester;
    this->section= sec;
    this->SeatNum = SeatNum;
}

Student::Student(Student& scopy):User(scopy)
{
      this->Semester= scopy.Semester;
      this->section = scopy.section;
      this->SeatNum = scopy.SeatNum;

}

Student::~Student()
{

}


int Student::getSemester()
{
    return this->Semester;
}

string Student::getsection()
{
     return this->section;
}


ofstream& operator << (ofstream& OutFile, Student S)
{
    OutFile <<  S.ConvertToUser();
    OutFile << S.Semester << endl;
    OutFile << S.section << endl;
    OutFile << S.SeatNum << endl;
    return OutFile;
}
ifstream& operator >> (ifstream& InFile, Student& S)
{
    InFile >> S.ConvertToUser();
    InFile >> S.Semester;
    InFile >> S.section;
    InFile >> S.SeatNum;
    return InFile;

}
