#-------------------------------------------------
#
# Project created by QtCreator 2018-06-20T09:43:19
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = timetable
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


#user_class/classrep.cpp \
#user_class/professor.cpp \
#user_class/coursesupervisor.cpp \

#  user_class/classrep.h \
#    user_class/professor.h \
#    user_class/coursesupervisor.h \




SOURCES += \
        main.cpp \
    login.cpp \
    sign_up.cpp \
    loadscreen.cpp \
    forget_pass.cpp \
    time_table_manager.cpp \
    TableClass/session.cpp \
    TableClass/day.cpp \
    TableClass/table.cpp \
    user_class/student.cpp \
    user_class/user.cpp \
    student_client.cpp \
    mainwindow.cpp \
    newtable.cpp \
    info.cpp \
    Misc/sessiondictionary.cpp

HEADERS += \
    login.h \
    sign_up.h \
    loadscreen.h \
    forget_pass.h \
    time_table_manager.h \
    TableClass/session.h \
    TableClass/day.h \
    TableClass/table.h \
    user_class/student.h \
    user_class/user.h \
    student_client.h \
    mainwindow.h \
    newtable.h \
    info.h \
    Misc/sessiondictionary.h

FORMS += \
    login.ui \
    sign_up.ui \
    loadscreen.ui \
    forget_pass.ui \
    time_table_manager.ui \
    student_client.ui \
    mainwindow.ui \
    newtable.ui \
    info.ui
