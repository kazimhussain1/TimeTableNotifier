#ifndef NEWTABLE_H
#define NEWTABLE_H

#include <QWidget>
#include <QTableWidget>
#include <QDialog>
#include <bits/stdc++.h>

#include "info.h"
#include "TableClass/table.h"
#include "Misc/sessiondictionary.h"

namespace Ui {
class NewTable;
}

class NewTable : public QWidget
{
    Q_OBJECT

public:
    explicit NewTable(int row, int column, QWidget *parent = 0);
    ~NewTable();

public slots:
    void DoubleClickEvent(int row,int column);
private slots:


    void on_Submit_clicked();

private:
    Ui::NewTable *ui;
    QTableWidget *DisplayTable;
    Table *MyNewTable;

    vector<vector<SessionDictionary*> > *SessionArray;
    Info *myInfo;
};

#endif // NEWTABLE_H
