#include "forget_pass.h"
#include "ui_forget_pass.h"

forget_pass::forget_pass(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::forget_pass)
{
    ui->setupUi(this);
    ui->gridGroupBox1->hide();
    ui->gridGroupBox2->hide();
    ui->gridGroupBox3->hide();
    ui->label_Email->hide();
    ui->label_C_no->hide();
    ui->label_A_Info->hide();
    this->setFixedSize(1000,700);
}

forget_pass::~forget_pass()
{
    delete ui;
}

void forget_pass::on_F_acc_clicked()
{
    QString email,contact,A_type;
    email = ui->Email->text();
    contact = ui->C_no->text();
    A_type = ui->A_info->text();

    if(email == "test" && contact == "090078601" && A_type == "Time Table Manager")
    {
        ui->gridGroupBox1->show();
        ui->Code->setPlaceholderText("CODE");
    }
    else
    {
        ui->label_A_Info->show();
        ui->label_C_no->show();
        ui->label_Email->show();
    }
}

void forget_pass::on_S_code_clicked()
{
    ui->gridGroupBox2->show();
}

void forget_pass::on_S_question_clicked()
{
    ui->gridGroupBox3->show();
}

void forget_pass::on_N_password_clicked()
{
    this->hide();
}
