#include "student_client.h"
#include "ui_student_client.h"

StudentClient::StudentClient(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::StudentClient)
{
    ui->setupUi(this);

    DisplayTable = ui->tableWidget;

    this->setFixedSize(QSize(1200, 700));
    ui->gridGroupBox->hide();
    ui->label_profile->hide();
    ui->gridGroupBox1->hide();
    ui->label_setting->hide();
    ui->gridGroupBox2->hide();
    ui->label_P_notes->hide();
    ui->groupBox_3->hide();

    ifstream myFile("output.txt");

    int temp ;
    myFile>>temp;
    myFile.close();

    myFile.open("output.txt");

    myTable = new Table(temp);
    myFile >> *myTable;


    /*Day *tempDay = new Day(3);

    Session *tempSession = new Session("ALP_T", 401, "Urooj_Waheed", 2);
    tempDay->AddSession(*tempSession);

    delete tempSession;
    tempSession = new Session("OOP_T", 403, "Humera_Tariq", 2);
    tempDay->AddSession(*tempSession);

    delete tempSession;
    tempSession = new Session("OOP_L", 403, "Humera_Tariq", 2);
    tempDay->AddSession(*tempSession);


    myTable->AddDay(*tempDay);
    myTable->AddDay(*tempDay);
    myTable->AddDay(*tempDay);
    myTable->AddDay(*tempDay);

    delete tempDay;
    tempDay = new Day(3);

    delete tempSession;
    tempSession = new Session("OOP_T", 403, "Humera_Tariq", 3);
    tempDay->AddSession(*tempSession);

//    delete tempSession;
//    tempSession = new Session("OOP_L", 403, "Humera_Tariq", 1);
//    tempDay->AddSession(*tempSession);

    delete tempSession;
    tempSession = new Session("SCMD_L", 407, "Farid_Alvi", 1);
    tempDay->AddSession(*tempSession);

    delete tempSession;
    tempSession = new Session("SCMD_T", 407, "Farid_Alvi", 2);
    tempDay->AddSession(*tempSession);

    myTable->AddDay(*tempDay);*/






    myTable->display(DisplayTable, this);

    myFile.close();


}

StudentClient::~StudentClient()
{
    delete ui;
}

void StudentClient::on_P_edit_clicked()
{
    ui->gridGroupBox1->show();
    ui->label_setting->show();
    ui->gridGroupBox->hide();
    ui->label_profile->hide();
    ui->gridGroupBox2->hide();
    ui->label_P_notes->hide();
    ui->C_pass->hide();
    ui->C_pass1->hide();
}

void StudentClient::on_Profile_clicked()
{
    ui->gridGroupBox->show();
    ui->label_profile->show();
    ui->gridGroupBox1->hide();
    ui->label_setting->hide();
    ui->gridGroupBox2->hide();
    ui->label_P_notes->hide();
    ui->groupBox_3->hide();
}

void StudentClient::on_Setting_clicked()
{
    ui->gridGroupBox1->show();
    ui->label_setting->show();
    ui->gridGroupBox->hide();
    ui->label_profile->hide();
    ui->gridGroupBox2->hide();
    ui->label_P_notes->hide();
    ui->plainTextEdit_2->hide();
    ui->C_pass->hide();
    ui->C_pass1->hide();
    ui->groupBox_3->hide();
}

void StudentClient::on_P_notes_clicked()
{
    ui->gridGroupBox->hide();
    ui->label_profile->hide();
    ui->gridGroupBox1->hide();
    ui->label_setting->hide();
    ui->gridGroupBox2->show();
    ui->label_P_notes->show();
    ui->groupBox_3->hide();

}

void StudentClient::on_add_2_clicked()
{
    ui->plainTextEdit_2->show();
    this->setGeometry(340,40,75,23);
}

void StudentClient::on_E_pass_clicked()
{
    ui->C_pass->show();
    ui->C_pass1->show();
    ui->Sem->setGeometry(60,245,51,31);
    ui->Semester->setGeometry(150,250,151,26);
    ui->E_sem->setGeometry(310,250,41,21);
    ui->l_Theme->setGeometry(60,285,51,31);
    ui->Theme->setGeometry(150,290,151,21);
    ui->E_theme->setGeometry(310,290,41,21);
}

void StudentClient::on_T_table_clicked()
{
    ui->groupBox_3->show();
    ui->gridGroupBox->hide();
    ui->label_profile->hide();
    ui->gridGroupBox1->hide();
    ui->label_setting->hide();
    ui->gridGroupBox2->hide();
    ui->label_P_notes->hide();
}
